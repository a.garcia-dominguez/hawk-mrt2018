#!/bin/bash

set -e

find_repo_version() {
    ls target/*.zip \
        | head -1 \
        | sed -r 's/.*eclipse-repository-(.*)-SNAPSHOT.zip/\1/'
}

bintray_curl() {
    curl -u "$BINTRAY_API_USER:$BINTRAY_API_KEY" "$@"
    RESULT=$?
    echo
    return $RESULT
}

upload_file() {
    BASEDIR="$1"
    FILE="$2"
    BINTRAY_PATH="$REPO_VERSION/${FILE#$BASEDIR/}"

    # Delete old file (Bintray does not allow for overwriting). Note that Bintray limits
    # file deletions 180 days beyond publishing (would need to raise the version number).
    echo "Deleting $BINTRAY_PATH (in case we want to overwrite)"
    bintray_curl -X DELETE "$BINTRAY_BASE_URL/$BINTRAY_PATH"

    # We need to use matrix parameters to reliably place things in a certain path
    URL="$BINTRAY_BASE_URL/$BINTRAY_PATH;bt_package=$BINTRAY_REPO_NAME;bt_version=$REPO_VERSION"
    echo "Uploading '$FILE' to '$BINTRAY_PATH'"
    bintray_curl -T "$FILE" "$URL"
}

publish_version() {
    bintray_curl -X POST "$BINTRAY_BASE_URL/$BINTRAY_REPO_NAME/$REPO_VERSION/publish"
}

export REPO_DIR=target/repository
export REPO_VERSION=$(find_repo_version)

if ! test -d "$REPO_DIR"; then
    echo "Repo directory $REPO_DIR does not exist"
    exit 1
elif test -z "$REPO_VERSION"; then
    echo "Could not find out the repository version"
    exit 2
elif test -z "$BINTRAY_API_KEY"; then
    echo "The BINTRAY_API_KEY environment variable has not been set"
    exit 3
elif test -z "$BINTRAY_BASE_URL"; then
    echo "The BINTRAY_BASE_URL environment variable has not been set"
    exit 4
elif test -z "$BINTRAY_API_USER"; then
    echo "The BINTRAY_API_USER environment variable has not been set"
    exit 5
elif test -z "$BINTRAY_REPO_NAME"; then
    echo "The BINTRAY_REPO_NAME environment variable has not been set"
    exit 6
fi

find "$REPO_DIR" -type f | (
    while read f; do
        upload_file "$REPO_DIR" "$f"
    done
)

publish_version
