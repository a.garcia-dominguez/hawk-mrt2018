/*******************************************************************************
 * Copyright (c) 2019 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawk.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.Socket;
import java.util.Collection;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.ICredentialsStore.Credentials;
import org.eclipse.hawk.core.util.DefaultConsole;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;

/**
 * Tests for the {@link MongoCollection} class.
 */
public class MongoCollectionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private static final String DB_NAME = "hawktest";
	private static final String COLLECTION_NAME = "test";
	private static final String MONGO_URL = String.format(
		"mongo://localhost:27017/%s/%s", DB_NAME, COLLECTION_NAME
	);

	private ICredentialsStore credStore;
	private MongoCollection vcs = new MongoCollection();
	private com.mongodb.client.MongoCollection<Document> mongoCollection;

	@Before
	public void setUp() throws Exception {
		// Keeps Tycho happy in GitLab
		assumeTrue(isPortInUse("localhost", 27017));

		// Clear collection between tests
		MongoDatabase mongoDB = MongoClients.create().getDatabase(DB_NAME);
		mongoCollection = mongoDB.getCollection(COLLECTION_NAME);
		mongoCollection.deleteMany(new Document());
		
		credStore = mock(ICredentialsStore.class);
		when(credStore.get(anyString())).thenReturn(new Credentials("dummy", "dummy"));

		IModelIndexer indexer = mock(IModelIndexer.class);
		when(indexer.getConsole()).thenReturn(new DefaultConsole());
		when(indexer.getCredentialsStore()).thenReturn(credStore);

		vcs = new MongoCollection();
		vcs.init(MONGO_URL, indexer);
		vcs.run();
		assertTrue(vcs.isActive());
		assertFalse(vcs.isFrozen());
	}

	@After
	public void tearDown() {
		if (vcs != null) {
			vcs.shutdown();
		}
	}

	@Test
	public void canHandleSingleDocument() throws Exception {
		final long tstamp = System.currentTimeMillis();
		mongoCollection.insertOne(new Document()
			.append(MongoCollection.ATTR_LAST_UPDATED, tstamp)
			.append("label", "A")
		);
		assertEquals(tstamp + "", vcs.getCurrentRevision());

		final Collection<VcsCommitItem> delta = vcs.getDelta(vcs.getFirstRevision());
		assertEquals(1, delta.size());
		final VcsCommitItem firstItem = delta.iterator().next();
		assertEquals(VcsChangeType.UPDATED, firstItem.getChangeType());

		final File f = vcs.importFile(
			firstItem.getCommit().getRevision(),
			firstItem.getPath(),
			tempFolder.newFile()
		);
		boolean foundAttribute = false;
		try (FileReader fr = new FileReader(f); BufferedReader br = new BufferedReader(fr)) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("label")) {
					foundAttribute = true;
				}
			}
		}
		if (!foundAttribute) {
			fail("Did not find expected string in imported JSON document");
		}
	}

	@Test
	public void canHandleDeletion() throws Exception {
		final long tstamp = System.currentTimeMillis();
		mongoCollection.insertOne(new Document()
			.append(MongoCollection.ATTR_LAST_UPDATED, tstamp)
			.append("label", "A")
		);
		assertEquals(tstamp + "", vcs.getCurrentRevision());

		final Collection<VcsCommitItem> deltaAdded = vcs.getDelta(vcs.getFirstRevision());
		assertEquals(1, deltaAdded.size());
		final VcsCommitItem firstItem = deltaAdded.iterator().next();
		assertEquals(VcsChangeType.UPDATED, firstItem.getChangeType());

		mongoCollection.deleteMany(new Document());
		Collection<VcsCommitItem> deltaDeleted = vcs.getDelta(firstItem.getCommit().getRevision());
		assertEquals(1, deltaDeleted.size());
		assertEquals(VcsChangeType.DELETED, deltaDeleted.iterator().next().getChangeType());
	}

	@Test
	public void canHandleUpdate() throws Exception {
		final long tstamp = System.currentTimeMillis();
		mongoCollection.insertOne(new Document()
			.append(MongoCollection.ATTR_LAST_UPDATED, tstamp)
			.append("label", "A")
		);
		assertEquals(tstamp + "", vcs.getCurrentRevision());

		final Collection<VcsCommitItem> deltaAdded = vcs.getDelta(vcs.getFirstRevision());
		assertEquals(1, deltaAdded.size());
		final VcsCommitItem firstItem = deltaAdded.iterator().next();
		assertEquals(VcsChangeType.UPDATED, firstItem.getChangeType());

		mongoCollection.findOneAndUpdate(
			new Document("_id", new ObjectId(firstItem.getPath().substring(1))),
			Updates.combine(
				Updates.set("label", "B"),
				Updates.set(MongoCollection.ATTR_LAST_UPDATED, System.currentTimeMillis())
			)
		);
		Collection<VcsCommitItem> deltaDeleted = vcs.getDelta(firstItem.getCommit().getRevision());
		assertEquals(1, deltaDeleted.size());
		assertEquals(VcsChangeType.UPDATED, deltaDeleted.iterator().next().getChangeType());
	}

	private boolean isPortInUse(String hostName, int portNumber) {
        try (Socket s = new Socket(hostName, portNumber)) {
            return true;
        }
        catch (Exception e) {
            return false;
        }
	}
}
