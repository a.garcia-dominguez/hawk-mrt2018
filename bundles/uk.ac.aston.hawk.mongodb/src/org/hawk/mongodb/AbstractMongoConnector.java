/*******************************************************************************
 * Copyright (c) 2019 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.hawk.mongodb;

import java.net.URI;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.core.ICredentialsStore.Credentials;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClientSettings.Builder;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

/**
 * Base class for any MongoDB-based connector.
 */
public abstract class AbstractMongoConnector implements IVcsManager {

	protected IConsole console;
	protected IModelIndexer indexer;

	private boolean isFrozen = false;
	private boolean isActive = false;

	private String repositoryURL;
	private String username;
	private String password;
	private String mongoURL;
	private String mongoHost;
	private int mongoPort;

	private String mongoDatabaseName;
	private MongoClient mongoClient;
	private CountDownLatch latch;
	protected MongoDatabase mongoDatabase;

	public AbstractMongoConnector() {
		super();
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void init(String vcsloc, IModelIndexer hawk) throws Exception {
		this.indexer = hawk;
		this.mongoURL = vcsloc;
		console = indexer.getConsole();

		final URI mongoURI = new URI(vcsloc);
		this.mongoHost = mongoURI.getHost();
		this.mongoPort = mongoURI.getPort();
		String[] pathParts = mongoURI.getPath().split("/");
		if (pathParts.length < 2) {
			throw new IllegalArgumentException("URL should have /db/collection as its path (Exampe: mongo://localhost:27017/db)");
		}
		this.mongoDatabaseName = pathParts[1];
	}

	@Override
	public void run() throws Exception {
		final ICredentialsStore credStore = indexer.getCredentialsStore();
		if (username != null) {
			// The credentials were provided by a previous setCredentials
			// call: retry the change to the credentials store.
			setCredentials(username, password, credStore);
		} else {
			final Credentials credentials = credStore.get(repositoryURL);
			if (credentials != null) {
				this.username = credentials.getUsername();
				this.password = credentials.getPassword();
			} else {
				/*
				 * If we use null for the default username/password, SVNKit
				 * will try to use the GNOME keyring in Linux, and that will
				 * lock up our Eclipse instance in some cases.
				 */
				console.printerrln("No username/password recorded for the repository " + repositoryURL);
				this.username = "";
				this.password = "";
			}
		}
		
		Builder builder = MongoClientSettings.builder().applyToClusterSettings(
			b -> b.hosts(Arrays.asList(new ServerAddress(mongoHost, mongoPort)))
		);

		if (username != null && !"".equals(username)) {
			MongoCredential cred = MongoCredential.createCredential(username, mongoDatabaseName, password.toCharArray());
			builder.credential(cred);
		}
		latch = new CountDownLatch(1);
		this.mongoClient = MongoClients.create(builder.build());
		
		this.mongoDatabase = mongoClient.getDatabase(mongoDatabaseName);
		isActive = true;
	}

	@Override
	public void shutdown() {
		// cannot release MongoDB connection explicitly, it seems!
		isActive = false;
		if (latch != null) {
			latch.countDown();
		}
	}
	

	@Override
	public void setCredentials(String username, String password, ICredentialsStore credStore) {
		if (username != null && password != null && repositoryURL != null
				&& (!username.equals(this.username) || !password.equals(this.password))) {
			try {
				credStore.put(repositoryURL, new Credentials(username, password));
			} catch (Exception e) {
				console.printerrln("Could not save new username/password");
				console.printerrln(e);
			}
		}
		this.username = username;
		this.password = password;
	}

	@Override
	public boolean isAuthSupported() {
		return true;
	}

	@Override
	public boolean isPathLocationAccepted() {
		return false;
	}

	@Override
	public boolean isURLLocationAccepted() {
		return true;
	}

	@Override
	public boolean isFrozen() {
		return isFrozen;
	}

	@Override
	public void setFrozen(boolean frozen) {
		isFrozen = frozen;
	}

	@Override
	public String getLocation() {
		return mongoURL;
	}

	public String getDefaultLocation() {
		return "mongo://localhost:27017/dbname";
	}

}