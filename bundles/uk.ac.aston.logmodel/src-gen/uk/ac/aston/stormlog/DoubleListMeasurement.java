/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double List Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.DoubleListMeasurement#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getDoubleListMeasurement()
 * @model
 * @generated
 */
public interface DoubleListMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDoubleListMeasurement_Value()
	 * @model unique="false"
	 * @generated
	 */
	EList<Double> getValue();

} // DoubleListMeasurement
