/**
 */
package uk.ac.aston.stormlog;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.RangeMeasurement#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getRangeMeasurement()
 * @model
 * @generated
 */
public interface RangeMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute.
	 * @see #setPosition(int)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRangeMeasurement_Position()
	 * @model unique="false"
	 * @generated
	 */
	int getPosition();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.RangeMeasurement#getPosition <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' attribute.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(int value);

} // RangeMeasurement
