/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFR Satisfaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.NFRSatisfaction#getNfr <em>Nfr</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.NFRSatisfaction#isSatisfied <em>Satisfied</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRSatisfaction()
 * @model
 * @generated
 */
public interface NFRSatisfaction extends EObject {
	/**
	 * Returns the value of the '<em><b>Nfr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nfr</em>' reference.
	 * @see #setNfr(NFR)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRSatisfaction_Nfr()
	 * @model
	 * @generated
	 */
	NFR getNfr();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.NFRSatisfaction#getNfr <em>Nfr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nfr</em>' reference.
	 * @see #getNfr()
	 * @generated
	 */
	void setNfr(NFR value);

	/**
	 * Returns the value of the '<em><b>Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied</em>' attribute.
	 * @see #setSatisfied(boolean)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRSatisfaction_Satisfied()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSatisfied();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.NFRSatisfaction#isSatisfied <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Satisfied</em>' attribute.
	 * @see #isSatisfied()
	 * @generated
	 */
	void setSatisfied(boolean value);

} // NFRSatisfaction
