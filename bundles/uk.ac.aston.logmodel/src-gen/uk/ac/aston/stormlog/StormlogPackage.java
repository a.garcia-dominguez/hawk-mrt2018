/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see uk.ac.aston.stormlog.StormlogFactory
 * @model kind="package"
 * @generated
 */
public interface StormlogPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "stormlog";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "uk.ac.aston.stormlog";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "stormlog";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StormlogPackage eINSTANCE = uk.ac.aston.stormlog.impl.StormlogPackageImpl.init();

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.LogImpl <em>Log</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.LogImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getLog()
	 * @generated
	 */
	int LOG = 0;

	/**
	 * The feature id for the '<em><b>Timeslice ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__TIMESLICE_ID = 0;

	/**
	 * The feature id for the '<em><b>Decisions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__DECISIONS = 1;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__ACTIONS = 2;

	/**
	 * The feature id for the '<em><b>Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__REQUIREMENTS = 3;

	/**
	 * The feature id for the '<em><b>Measures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__MEASURES = 4;

	/**
	 * The feature id for the '<em><b>Observations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__OBSERVATIONS = 5;

	/**
	 * The number of structural features of the '<em>Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Get Measure</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG___GET_MEASURE__STRING = 0;

	/**
	 * The operation id for the '<em>Get Action</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG___GET_ACTION__STRING = 1;

	/**
	 * The number of operations of the '<em>Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.DecisionImpl <em>Decision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.DecisionImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDecision()
	 * @generated
	 */
	int DECISION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Reward Tables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__REWARD_TABLES = 1;

	/**
	 * The feature id for the '<em><b>Action Beliefs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__ACTION_BELIEFS = 2;

	/**
	 * The feature id for the '<em><b>Observation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__OBSERVATION = 3;

	/**
	 * The feature id for the '<em><b>Action Taken</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__ACTION_TAKEN = 4;

	/**
	 * The feature id for the '<em><b>Actual Reward</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION__ACTUAL_REWARD = 5;

	/**
	 * The number of structural features of the '<em>Decision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Decision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.ObservationImpl <em>Observation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.ObservationImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getObservation()
	 * @generated
	 */
	int OBSERVATION = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Probability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION__PROBABILITY = 1;

	/**
	 * The feature id for the '<em><b>Measurements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION__MEASUREMENTS = 2;

	/**
	 * The number of structural features of the '<em>Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get First Measurement</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION___GET_FIRST_MEASUREMENT__STRING = 0;

	/**
	 * The operation id for the '<em>Get First Measurement</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION___GET_FIRST_MEASUREMENT__MEASURE = 1;

	/**
	 * The number of operations of the '<em>Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVATION_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.MeasurementImpl <em>Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.MeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getMeasurement()
	 * @generated
	 */
	int MEASUREMENT = 3;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__MEASURE = 0;

	/**
	 * The number of structural features of the '<em>Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.MeasureImpl <em>Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.MeasureImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getMeasure()
	 * @generated
	 */
	int MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Thresholds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__THRESHOLDS = 1;

	/**
	 * The number of structural features of the '<em>Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.ThresholdImpl <em>Threshold</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.ThresholdImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getThreshold()
	 * @generated
	 */
	int THRESHOLD = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Threshold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Threshold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.ActionBeliefImpl <em>Action Belief</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.ActionBeliefImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getActionBelief()
	 * @generated
	 */
	int ACTION_BELIEF = 6;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BELIEF__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Estimated Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BELIEF__ESTIMATED_VALUE = 1;

	/**
	 * The number of structural features of the '<em>Action Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BELIEF_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BELIEF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.ActionImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = 0;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.NFRImpl <em>NFR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.NFRImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getNFR()
	 * @generated
	 */
	int NFR = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR__NAME = 0;

	/**
	 * The feature id for the '<em><b>Measures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR__MEASURES = 1;

	/**
	 * The number of structural features of the '<em>NFR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>NFR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.RewardTableImpl <em>Reward Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.RewardTableImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTable()
	 * @generated
	 */
	int REWARD_TABLE = 9;

	/**
	 * The feature id for the '<em><b>Rows</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE__ROWS = 0;

	/**
	 * The feature id for the '<em><b>Thresholds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE__THRESHOLDS = 1;

	/**
	 * The number of structural features of the '<em>Reward Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reward Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.NFRSatisfactionImpl <em>NFR Satisfaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.NFRSatisfactionImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getNFRSatisfaction()
	 * @generated
	 */
	int NFR_SATISFACTION = 10;

	/**
	 * The feature id for the '<em><b>Nfr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_SATISFACTION__NFR = 0;

	/**
	 * The feature id for the '<em><b>Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_SATISFACTION__SATISFIED = 1;

	/**
	 * The number of structural features of the '<em>NFR Satisfaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_SATISFACTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>NFR Satisfaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NFR_SATISFACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.RewardTableRowImpl <em>Reward Table Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.RewardTableRowImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTableRow()
	 * @generated
	 */
	int REWARD_TABLE_ROW = 11;

	/**
	 * The feature id for the '<em><b>Satisfactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_ROW__SATISFACTIONS = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_ROW__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_ROW__VALUE = 2;

	/**
	 * The number of structural features of the '<em>Reward Table Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_ROW_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Reward Table Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_ROW_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.RewardTableThresholdImpl <em>Reward Table Threshold</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.RewardTableThresholdImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTableThreshold()
	 * @generated
	 */
	int REWARD_TABLE_THRESHOLD = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_THRESHOLD__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Nfr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_THRESHOLD__NFR = 1;

	/**
	 * The number of structural features of the '<em>Reward Table Threshold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_THRESHOLD_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reward Table Threshold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_TABLE_THRESHOLD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.DoubleMeasurementImpl <em>Double Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.DoubleMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDoubleMeasurement()
	 * @generated
	 */
	int DOUBLE_MEASUREMENT = 13;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Double Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Double Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.IntegerMeasurementImpl <em>Integer Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.IntegerMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getIntegerMeasurement()
	 * @generated
	 */
	int INTEGER_MEASUREMENT = 14;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.StringMeasurementImpl <em>String Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.StringMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getStringMeasurement()
	 * @generated
	 */
	int STRING_MEASUREMENT = 15;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.RangeMeasurementImpl <em>Range Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.RangeMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRangeMeasurement()
	 * @generated
	 */
	int RANGE_MEASUREMENT = 16;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_MEASUREMENT__POSITION = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Range Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Range Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.DoubleListMeasurementImpl <em>Double List Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.DoubleListMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDoubleListMeasurement()
	 * @generated
	 */
	int DOUBLE_LIST_MEASUREMENT = 17;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_LIST_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_LIST_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Double List Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_LIST_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Double List Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_LIST_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.IntegerListMeasurementImpl <em>Integer List Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.IntegerListMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getIntegerListMeasurement()
	 * @generated
	 */
	int INTEGER_LIST_MEASUREMENT = 18;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LIST_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LIST_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer List Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LIST_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer List Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LIST_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link uk.ac.aston.stormlog.impl.BooleanMeasurementImpl <em>Boolean Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see uk.ac.aston.stormlog.impl.BooleanMeasurementImpl
	 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getBooleanMeasurement()
	 * @generated
	 */
	int BOOLEAN_MEASUREMENT = 19;

	/**
	 * The feature id for the '<em><b>Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_MEASUREMENT__MEASURE = MEASUREMENT__MEASURE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_MEASUREMENT__VALUE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Log <em>Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Log</em>'.
	 * @see uk.ac.aston.stormlog.Log
	 * @generated
	 */
	EClass getLog();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Log#getTimesliceID <em>Timeslice ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timeslice ID</em>'.
	 * @see uk.ac.aston.stormlog.Log#getTimesliceID()
	 * @see #getLog()
	 * @generated
	 */
	EAttribute getLog_TimesliceID();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Log#getDecisions <em>Decisions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decisions</em>'.
	 * @see uk.ac.aston.stormlog.Log#getDecisions()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Decisions();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Log#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see uk.ac.aston.stormlog.Log#getActions()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Actions();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Log#getRequirements <em>Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requirements</em>'.
	 * @see uk.ac.aston.stormlog.Log#getRequirements()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Requirements();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Log#getMeasures <em>Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Measures</em>'.
	 * @see uk.ac.aston.stormlog.Log#getMeasures()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Measures();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Log#getObservations <em>Observations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Observations</em>'.
	 * @see uk.ac.aston.stormlog.Log#getObservations()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Observations();

	/**
	 * Returns the meta object for the '{@link uk.ac.aston.stormlog.Log#getMeasure(java.lang.String) <em>Get Measure</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Measure</em>' operation.
	 * @see uk.ac.aston.stormlog.Log#getMeasure(java.lang.String)
	 * @generated
	 */
	EOperation getLog__GetMeasure__String();

	/**
	 * Returns the meta object for the '{@link uk.ac.aston.stormlog.Log#getAction(java.lang.String) <em>Get Action</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Action</em>' operation.
	 * @see uk.ac.aston.stormlog.Log#getAction(java.lang.String)
	 * @generated
	 */
	EOperation getLog__GetAction__String();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Decision <em>Decision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decision</em>'.
	 * @see uk.ac.aston.stormlog.Decision
	 * @generated
	 */
	EClass getDecision();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Decision#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getName()
	 * @see #getDecision()
	 * @generated
	 */
	EAttribute getDecision_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Decision#getRewardTables <em>Reward Tables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reward Tables</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getRewardTables()
	 * @see #getDecision()
	 * @generated
	 */
	EReference getDecision_RewardTables();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Decision#getActionBeliefs <em>Action Beliefs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action Beliefs</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getActionBeliefs()
	 * @see #getDecision()
	 * @generated
	 */
	EReference getDecision_ActionBeliefs();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.Decision#getObservation <em>Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observation</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getObservation()
	 * @see #getDecision()
	 * @generated
	 */
	EReference getDecision_Observation();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.Decision#getActionTaken <em>Action Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action Taken</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getActionTaken()
	 * @see #getDecision()
	 * @generated
	 */
	EReference getDecision_ActionTaken();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.Decision#getActualReward <em>Actual Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Actual Reward</em>'.
	 * @see uk.ac.aston.stormlog.Decision#getActualReward()
	 * @see #getDecision()
	 * @generated
	 */
	EReference getDecision_ActualReward();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Observation <em>Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observation</em>'.
	 * @see uk.ac.aston.stormlog.Observation
	 * @generated
	 */
	EClass getObservation();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Observation#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see uk.ac.aston.stormlog.Observation#getDescription()
	 * @see #getObservation()
	 * @generated
	 */
	EAttribute getObservation_Description();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Observation#getProbability <em>Probability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Probability</em>'.
	 * @see uk.ac.aston.stormlog.Observation#getProbability()
	 * @see #getObservation()
	 * @generated
	 */
	EAttribute getObservation_Probability();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Observation#getMeasurements <em>Measurements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Measurements</em>'.
	 * @see uk.ac.aston.stormlog.Observation#getMeasurements()
	 * @see #getObservation()
	 * @generated
	 */
	EReference getObservation_Measurements();

	/**
	 * Returns the meta object for the '{@link uk.ac.aston.stormlog.Observation#getFirstMeasurement(java.lang.String) <em>Get First Measurement</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get First Measurement</em>' operation.
	 * @see uk.ac.aston.stormlog.Observation#getFirstMeasurement(java.lang.String)
	 * @generated
	 */
	EOperation getObservation__GetFirstMeasurement__String();

	/**
	 * Returns the meta object for the '{@link uk.ac.aston.stormlog.Observation#getFirstMeasurement(uk.ac.aston.stormlog.Measure) <em>Get First Measurement</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get First Measurement</em>' operation.
	 * @see uk.ac.aston.stormlog.Observation#getFirstMeasurement(uk.ac.aston.stormlog.Measure)
	 * @generated
	 */
	EOperation getObservation__GetFirstMeasurement__Measure();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Measurement <em>Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Measurement</em>'.
	 * @see uk.ac.aston.stormlog.Measurement
	 * @generated
	 */
	EClass getMeasurement();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.Measurement#getMeasure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Measure</em>'.
	 * @see uk.ac.aston.stormlog.Measurement#getMeasure()
	 * @see #getMeasurement()
	 * @generated
	 */
	EReference getMeasurement_Measure();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Measure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Measure</em>'.
	 * @see uk.ac.aston.stormlog.Measure
	 * @generated
	 */
	EClass getMeasure();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Measure#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see uk.ac.aston.stormlog.Measure#getName()
	 * @see #getMeasure()
	 * @generated
	 */
	EAttribute getMeasure_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.Measure#getThresholds <em>Thresholds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Thresholds</em>'.
	 * @see uk.ac.aston.stormlog.Measure#getThresholds()
	 * @see #getMeasure()
	 * @generated
	 */
	EReference getMeasure_Thresholds();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Threshold <em>Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Threshold</em>'.
	 * @see uk.ac.aston.stormlog.Threshold
	 * @generated
	 */
	EClass getThreshold();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Threshold#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see uk.ac.aston.stormlog.Threshold#getName()
	 * @see #getThreshold()
	 * @generated
	 */
	EAttribute getThreshold_Name();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Threshold#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.Threshold#getValue()
	 * @see #getThreshold()
	 * @generated
	 */
	EAttribute getThreshold_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.ActionBelief <em>Action Belief</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Belief</em>'.
	 * @see uk.ac.aston.stormlog.ActionBelief
	 * @generated
	 */
	EClass getActionBelief();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.ActionBelief#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see uk.ac.aston.stormlog.ActionBelief#getAction()
	 * @see #getActionBelief()
	 * @generated
	 */
	EReference getActionBelief_Action();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.ActionBelief#getEstimatedValue <em>Estimated Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Estimated Value</em>'.
	 * @see uk.ac.aston.stormlog.ActionBelief#getEstimatedValue()
	 * @see #getActionBelief()
	 * @generated
	 */
	EAttribute getActionBelief_EstimatedValue();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see uk.ac.aston.stormlog.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.Action#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see uk.ac.aston.stormlog.Action#getName()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Name();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.NFR <em>NFR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NFR</em>'.
	 * @see uk.ac.aston.stormlog.NFR
	 * @generated
	 */
	EClass getNFR();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.NFR#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see uk.ac.aston.stormlog.NFR#getName()
	 * @see #getNFR()
	 * @generated
	 */
	EAttribute getNFR_Name();

	/**
	 * Returns the meta object for the reference list '{@link uk.ac.aston.stormlog.NFR#getMeasures <em>Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Measures</em>'.
	 * @see uk.ac.aston.stormlog.NFR#getMeasures()
	 * @see #getNFR()
	 * @generated
	 */
	EReference getNFR_Measures();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.RewardTable <em>Reward Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reward Table</em>'.
	 * @see uk.ac.aston.stormlog.RewardTable
	 * @generated
	 */
	EClass getRewardTable();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.RewardTable#getRows <em>Rows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rows</em>'.
	 * @see uk.ac.aston.stormlog.RewardTable#getRows()
	 * @see #getRewardTable()
	 * @generated
	 */
	EReference getRewardTable_Rows();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.RewardTable#getThresholds <em>Thresholds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Thresholds</em>'.
	 * @see uk.ac.aston.stormlog.RewardTable#getThresholds()
	 * @see #getRewardTable()
	 * @generated
	 */
	EReference getRewardTable_Thresholds();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.NFRSatisfaction <em>NFR Satisfaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NFR Satisfaction</em>'.
	 * @see uk.ac.aston.stormlog.NFRSatisfaction
	 * @generated
	 */
	EClass getNFRSatisfaction();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.NFRSatisfaction#getNfr <em>Nfr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Nfr</em>'.
	 * @see uk.ac.aston.stormlog.NFRSatisfaction#getNfr()
	 * @see #getNFRSatisfaction()
	 * @generated
	 */
	EReference getNFRSatisfaction_Nfr();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.NFRSatisfaction#isSatisfied <em>Satisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Satisfied</em>'.
	 * @see uk.ac.aston.stormlog.NFRSatisfaction#isSatisfied()
	 * @see #getNFRSatisfaction()
	 * @generated
	 */
	EAttribute getNFRSatisfaction_Satisfied();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.RewardTableRow <em>Reward Table Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reward Table Row</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableRow
	 * @generated
	 */
	EClass getRewardTableRow();

	/**
	 * Returns the meta object for the containment reference list '{@link uk.ac.aston.stormlog.RewardTableRow#getSatisfactions <em>Satisfactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Satisfactions</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableRow#getSatisfactions()
	 * @see #getRewardTableRow()
	 * @generated
	 */
	EReference getRewardTableRow_Satisfactions();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.RewardTableRow#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableRow#getAction()
	 * @see #getRewardTableRow()
	 * @generated
	 */
	EReference getRewardTableRow_Action();

	/**
	 * Returns the meta object for the attribute list '{@link uk.ac.aston.stormlog.RewardTableRow#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableRow#getValue()
	 * @see #getRewardTableRow()
	 * @generated
	 */
	EAttribute getRewardTableRow_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.RewardTableThreshold <em>Reward Table Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reward Table Threshold</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableThreshold
	 * @generated
	 */
	EClass getRewardTableThreshold();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.RewardTableThreshold#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableThreshold#getValue()
	 * @see #getRewardTableThreshold()
	 * @generated
	 */
	EAttribute getRewardTableThreshold_Value();

	/**
	 * Returns the meta object for the reference '{@link uk.ac.aston.stormlog.RewardTableThreshold#getNfr <em>Nfr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Nfr</em>'.
	 * @see uk.ac.aston.stormlog.RewardTableThreshold#getNfr()
	 * @see #getRewardTableThreshold()
	 * @generated
	 */
	EReference getRewardTableThreshold_Nfr();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.DoubleMeasurement <em>Double Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Measurement</em>'.
	 * @see uk.ac.aston.stormlog.DoubleMeasurement
	 * @generated
	 */
	EClass getDoubleMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.DoubleMeasurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.DoubleMeasurement#getValue()
	 * @see #getDoubleMeasurement()
	 * @generated
	 */
	EAttribute getDoubleMeasurement_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.IntegerMeasurement <em>Integer Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Measurement</em>'.
	 * @see uk.ac.aston.stormlog.IntegerMeasurement
	 * @generated
	 */
	EClass getIntegerMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.IntegerMeasurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.IntegerMeasurement#getValue()
	 * @see #getIntegerMeasurement()
	 * @generated
	 */
	EAttribute getIntegerMeasurement_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.StringMeasurement <em>String Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Measurement</em>'.
	 * @see uk.ac.aston.stormlog.StringMeasurement
	 * @generated
	 */
	EClass getStringMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.StringMeasurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.StringMeasurement#getValue()
	 * @see #getStringMeasurement()
	 * @generated
	 */
	EAttribute getStringMeasurement_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.RangeMeasurement <em>Range Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Measurement</em>'.
	 * @see uk.ac.aston.stormlog.RangeMeasurement
	 * @generated
	 */
	EClass getRangeMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.RangeMeasurement#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position</em>'.
	 * @see uk.ac.aston.stormlog.RangeMeasurement#getPosition()
	 * @see #getRangeMeasurement()
	 * @generated
	 */
	EAttribute getRangeMeasurement_Position();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.DoubleListMeasurement <em>Double List Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double List Measurement</em>'.
	 * @see uk.ac.aston.stormlog.DoubleListMeasurement
	 * @generated
	 */
	EClass getDoubleListMeasurement();

	/**
	 * Returns the meta object for the attribute list '{@link uk.ac.aston.stormlog.DoubleListMeasurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.DoubleListMeasurement#getValue()
	 * @see #getDoubleListMeasurement()
	 * @generated
	 */
	EAttribute getDoubleListMeasurement_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.IntegerListMeasurement <em>Integer List Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer List Measurement</em>'.
	 * @see uk.ac.aston.stormlog.IntegerListMeasurement
	 * @generated
	 */
	EClass getIntegerListMeasurement();

	/**
	 * Returns the meta object for the attribute list '{@link uk.ac.aston.stormlog.IntegerListMeasurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.IntegerListMeasurement#getValue()
	 * @see #getIntegerListMeasurement()
	 * @generated
	 */
	EAttribute getIntegerListMeasurement_Value();

	/**
	 * Returns the meta object for class '{@link uk.ac.aston.stormlog.BooleanMeasurement <em>Boolean Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Measurement</em>'.
	 * @see uk.ac.aston.stormlog.BooleanMeasurement
	 * @generated
	 */
	EClass getBooleanMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link uk.ac.aston.stormlog.BooleanMeasurement#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see uk.ac.aston.stormlog.BooleanMeasurement#isValue()
	 * @see #getBooleanMeasurement()
	 * @generated
	 */
	EAttribute getBooleanMeasurement_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StormlogFactory getStormlogFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.LogImpl <em>Log</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.LogImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getLog()
		 * @generated
		 */
		EClass LOG = eINSTANCE.getLog();

		/**
		 * The meta object literal for the '<em><b>Timeslice ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOG__TIMESLICE_ID = eINSTANCE.getLog_TimesliceID();

		/**
		 * The meta object literal for the '<em><b>Decisions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__DECISIONS = eINSTANCE.getLog_Decisions();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__ACTIONS = eINSTANCE.getLog_Actions();

		/**
		 * The meta object literal for the '<em><b>Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__REQUIREMENTS = eINSTANCE.getLog_Requirements();

		/**
		 * The meta object literal for the '<em><b>Measures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__MEASURES = eINSTANCE.getLog_Measures();

		/**
		 * The meta object literal for the '<em><b>Observations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__OBSERVATIONS = eINSTANCE.getLog_Observations();

		/**
		 * The meta object literal for the '<em><b>Get Measure</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOG___GET_MEASURE__STRING = eINSTANCE.getLog__GetMeasure__String();

		/**
		 * The meta object literal for the '<em><b>Get Action</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOG___GET_ACTION__STRING = eINSTANCE.getLog__GetAction__String();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.DecisionImpl <em>Decision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.DecisionImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDecision()
		 * @generated
		 */
		EClass DECISION = eINSTANCE.getDecision();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECISION__NAME = eINSTANCE.getDecision_Name();

		/**
		 * The meta object literal for the '<em><b>Reward Tables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION__REWARD_TABLES = eINSTANCE.getDecision_RewardTables();

		/**
		 * The meta object literal for the '<em><b>Action Beliefs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION__ACTION_BELIEFS = eINSTANCE.getDecision_ActionBeliefs();

		/**
		 * The meta object literal for the '<em><b>Observation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION__OBSERVATION = eINSTANCE.getDecision_Observation();

		/**
		 * The meta object literal for the '<em><b>Action Taken</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION__ACTION_TAKEN = eINSTANCE.getDecision_ActionTaken();

		/**
		 * The meta object literal for the '<em><b>Actual Reward</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECISION__ACTUAL_REWARD = eINSTANCE.getDecision_ActualReward();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.ObservationImpl <em>Observation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.ObservationImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getObservation()
		 * @generated
		 */
		EClass OBSERVATION = eINSTANCE.getObservation();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVATION__DESCRIPTION = eINSTANCE.getObservation_Description();

		/**
		 * The meta object literal for the '<em><b>Probability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVATION__PROBABILITY = eINSTANCE.getObservation_Probability();

		/**
		 * The meta object literal for the '<em><b>Measurements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVATION__MEASUREMENTS = eINSTANCE.getObservation_Measurements();

		/**
		 * The meta object literal for the '<em><b>Get First Measurement</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVATION___GET_FIRST_MEASUREMENT__STRING = eINSTANCE.getObservation__GetFirstMeasurement__String();

		/**
		 * The meta object literal for the '<em><b>Get First Measurement</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVATION___GET_FIRST_MEASUREMENT__MEASURE = eINSTANCE.getObservation__GetFirstMeasurement__Measure();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.MeasurementImpl <em>Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.MeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getMeasurement()
		 * @generated
		 */
		EClass MEASUREMENT = eINSTANCE.getMeasurement();

		/**
		 * The meta object literal for the '<em><b>Measure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASUREMENT__MEASURE = eINSTANCE.getMeasurement_Measure();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.MeasureImpl <em>Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.MeasureImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getMeasure()
		 * @generated
		 */
		EClass MEASURE = eINSTANCE.getMeasure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE__NAME = eINSTANCE.getMeasure_Name();

		/**
		 * The meta object literal for the '<em><b>Thresholds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASURE__THRESHOLDS = eINSTANCE.getMeasure_Thresholds();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.ThresholdImpl <em>Threshold</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.ThresholdImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getThreshold()
		 * @generated
		 */
		EClass THRESHOLD = eINSTANCE.getThreshold();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLD__NAME = eINSTANCE.getThreshold_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THRESHOLD__VALUE = eINSTANCE.getThreshold_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.ActionBeliefImpl <em>Action Belief</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.ActionBeliefImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getActionBelief()
		 * @generated
		 */
		EClass ACTION_BELIEF = eINSTANCE.getActionBelief();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_BELIEF__ACTION = eINSTANCE.getActionBelief_Action();

		/**
		 * The meta object literal for the '<em><b>Estimated Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_BELIEF__ESTIMATED_VALUE = eINSTANCE.getActionBelief_EstimatedValue();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.ActionImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__NAME = eINSTANCE.getAction_Name();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.NFRImpl <em>NFR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.NFRImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getNFR()
		 * @generated
		 */
		EClass NFR = eINSTANCE.getNFR();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NFR__NAME = eINSTANCE.getNFR_Name();

		/**
		 * The meta object literal for the '<em><b>Measures</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NFR__MEASURES = eINSTANCE.getNFR_Measures();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.RewardTableImpl <em>Reward Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.RewardTableImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTable()
		 * @generated
		 */
		EClass REWARD_TABLE = eINSTANCE.getRewardTable();

		/**
		 * The meta object literal for the '<em><b>Rows</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REWARD_TABLE__ROWS = eINSTANCE.getRewardTable_Rows();

		/**
		 * The meta object literal for the '<em><b>Thresholds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REWARD_TABLE__THRESHOLDS = eINSTANCE.getRewardTable_Thresholds();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.NFRSatisfactionImpl <em>NFR Satisfaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.NFRSatisfactionImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getNFRSatisfaction()
		 * @generated
		 */
		EClass NFR_SATISFACTION = eINSTANCE.getNFRSatisfaction();

		/**
		 * The meta object literal for the '<em><b>Nfr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NFR_SATISFACTION__NFR = eINSTANCE.getNFRSatisfaction_Nfr();

		/**
		 * The meta object literal for the '<em><b>Satisfied</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NFR_SATISFACTION__SATISFIED = eINSTANCE.getNFRSatisfaction_Satisfied();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.RewardTableRowImpl <em>Reward Table Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.RewardTableRowImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTableRow()
		 * @generated
		 */
		EClass REWARD_TABLE_ROW = eINSTANCE.getRewardTableRow();

		/**
		 * The meta object literal for the '<em><b>Satisfactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REWARD_TABLE_ROW__SATISFACTIONS = eINSTANCE.getRewardTableRow_Satisfactions();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REWARD_TABLE_ROW__ACTION = eINSTANCE.getRewardTableRow_Action();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REWARD_TABLE_ROW__VALUE = eINSTANCE.getRewardTableRow_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.RewardTableThresholdImpl <em>Reward Table Threshold</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.RewardTableThresholdImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRewardTableThreshold()
		 * @generated
		 */
		EClass REWARD_TABLE_THRESHOLD = eINSTANCE.getRewardTableThreshold();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REWARD_TABLE_THRESHOLD__VALUE = eINSTANCE.getRewardTableThreshold_Value();

		/**
		 * The meta object literal for the '<em><b>Nfr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REWARD_TABLE_THRESHOLD__NFR = eINSTANCE.getRewardTableThreshold_Nfr();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.DoubleMeasurementImpl <em>Double Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.DoubleMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDoubleMeasurement()
		 * @generated
		 */
		EClass DOUBLE_MEASUREMENT = eINSTANCE.getDoubleMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_MEASUREMENT__VALUE = eINSTANCE.getDoubleMeasurement_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.IntegerMeasurementImpl <em>Integer Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.IntegerMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getIntegerMeasurement()
		 * @generated
		 */
		EClass INTEGER_MEASUREMENT = eINSTANCE.getIntegerMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_MEASUREMENT__VALUE = eINSTANCE.getIntegerMeasurement_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.StringMeasurementImpl <em>String Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.StringMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getStringMeasurement()
		 * @generated
		 */
		EClass STRING_MEASUREMENT = eINSTANCE.getStringMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_MEASUREMENT__VALUE = eINSTANCE.getStringMeasurement_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.RangeMeasurementImpl <em>Range Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.RangeMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getRangeMeasurement()
		 * @generated
		 */
		EClass RANGE_MEASUREMENT = eINSTANCE.getRangeMeasurement();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGE_MEASUREMENT__POSITION = eINSTANCE.getRangeMeasurement_Position();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.DoubleListMeasurementImpl <em>Double List Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.DoubleListMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getDoubleListMeasurement()
		 * @generated
		 */
		EClass DOUBLE_LIST_MEASUREMENT = eINSTANCE.getDoubleListMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_LIST_MEASUREMENT__VALUE = eINSTANCE.getDoubleListMeasurement_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.IntegerListMeasurementImpl <em>Integer List Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.IntegerListMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getIntegerListMeasurement()
		 * @generated
		 */
		EClass INTEGER_LIST_MEASUREMENT = eINSTANCE.getIntegerListMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_LIST_MEASUREMENT__VALUE = eINSTANCE.getIntegerListMeasurement_Value();

		/**
		 * The meta object literal for the '{@link uk.ac.aston.stormlog.impl.BooleanMeasurementImpl <em>Boolean Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see uk.ac.aston.stormlog.impl.BooleanMeasurementImpl
		 * @see uk.ac.aston.stormlog.impl.StormlogPackageImpl#getBooleanMeasurement()
		 * @generated
		 */
		EClass BOOLEAN_MEASUREMENT = eINSTANCE.getBooleanMeasurement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_MEASUREMENT__VALUE = eINSTANCE.getBooleanMeasurement_Value();

	}

} //StormlogPackage
