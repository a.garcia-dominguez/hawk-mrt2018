/**
 */
package uk.ac.aston.stormlog.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import uk.ac.aston.stormlog.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StormlogFactoryImpl extends EFactoryImpl implements StormlogFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StormlogFactory init() {
		try {
			StormlogFactory theStormlogFactory = (StormlogFactory)EPackage.Registry.INSTANCE.getEFactory(StormlogPackage.eNS_URI);
			if (theStormlogFactory != null) {
				return theStormlogFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StormlogFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StormlogFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StormlogPackage.LOG: return createLog();
			case StormlogPackage.DECISION: return createDecision();
			case StormlogPackage.OBSERVATION: return createObservation();
			case StormlogPackage.MEASURE: return createMeasure();
			case StormlogPackage.THRESHOLD: return createThreshold();
			case StormlogPackage.ACTION_BELIEF: return createActionBelief();
			case StormlogPackage.ACTION: return createAction();
			case StormlogPackage.NFR: return createNFR();
			case StormlogPackage.REWARD_TABLE: return createRewardTable();
			case StormlogPackage.NFR_SATISFACTION: return createNFRSatisfaction();
			case StormlogPackage.REWARD_TABLE_ROW: return createRewardTableRow();
			case StormlogPackage.REWARD_TABLE_THRESHOLD: return createRewardTableThreshold();
			case StormlogPackage.DOUBLE_MEASUREMENT: return createDoubleMeasurement();
			case StormlogPackage.INTEGER_MEASUREMENT: return createIntegerMeasurement();
			case StormlogPackage.STRING_MEASUREMENT: return createStringMeasurement();
			case StormlogPackage.RANGE_MEASUREMENT: return createRangeMeasurement();
			case StormlogPackage.DOUBLE_LIST_MEASUREMENT: return createDoubleListMeasurement();
			case StormlogPackage.INTEGER_LIST_MEASUREMENT: return createIntegerListMeasurement();
			case StormlogPackage.BOOLEAN_MEASUREMENT: return createBooleanMeasurement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Log createLog() {
		LogImpl log = new LogImpl();
		return log;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Decision createDecision() {
		DecisionImpl decision = new DecisionImpl();
		return decision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Observation createObservation() {
		ObservationImpl observation = new ObservationImpl();
		return observation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Measure createMeasure() {
		MeasureImpl measure = new MeasureImpl();
		return measure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Threshold createThreshold() {
		ThresholdImpl threshold = new ThresholdImpl();
		return threshold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActionBelief createActionBelief() {
		ActionBeliefImpl actionBelief = new ActionBeliefImpl();
		return actionBelief;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NFR createNFR() {
		NFRImpl nfr = new NFRImpl();
		return nfr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RewardTable createRewardTable() {
		RewardTableImpl rewardTable = new RewardTableImpl();
		return rewardTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NFRSatisfaction createNFRSatisfaction() {
		NFRSatisfactionImpl nfrSatisfaction = new NFRSatisfactionImpl();
		return nfrSatisfaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RewardTableRow createRewardTableRow() {
		RewardTableRowImpl rewardTableRow = new RewardTableRowImpl();
		return rewardTableRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RewardTableThreshold createRewardTableThreshold() {
		RewardTableThresholdImpl rewardTableThreshold = new RewardTableThresholdImpl();
		return rewardTableThreshold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DoubleMeasurement createDoubleMeasurement() {
		DoubleMeasurementImpl doubleMeasurement = new DoubleMeasurementImpl();
		return doubleMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntegerMeasurement createIntegerMeasurement() {
		IntegerMeasurementImpl integerMeasurement = new IntegerMeasurementImpl();
		return integerMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringMeasurement createStringMeasurement() {
		StringMeasurementImpl stringMeasurement = new StringMeasurementImpl();
		return stringMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RangeMeasurement createRangeMeasurement() {
		RangeMeasurementImpl rangeMeasurement = new RangeMeasurementImpl();
		return rangeMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DoubleListMeasurement createDoubleListMeasurement() {
		DoubleListMeasurementImpl doubleListMeasurement = new DoubleListMeasurementImpl();
		return doubleListMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntegerListMeasurement createIntegerListMeasurement() {
		IntegerListMeasurementImpl integerListMeasurement = new IntegerListMeasurementImpl();
		return integerListMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanMeasurement createBooleanMeasurement() {
		BooleanMeasurementImpl booleanMeasurement = new BooleanMeasurementImpl();
		return booleanMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StormlogPackage getStormlogPackage() {
		return (StormlogPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StormlogPackage getPackage() {
		return StormlogPackage.eINSTANCE;
	}

} //StormlogFactoryImpl
