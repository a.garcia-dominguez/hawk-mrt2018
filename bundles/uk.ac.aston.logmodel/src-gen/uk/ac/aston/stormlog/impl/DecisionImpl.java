/**
 */
package uk.ac.aston.stormlog.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.ActionBelief;
import uk.ac.aston.stormlog.Decision;
import uk.ac.aston.stormlog.Observation;
import uk.ac.aston.stormlog.RewardTable;
import uk.ac.aston.stormlog.RewardTableRow;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decision</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getName <em>Name</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getRewardTables <em>Reward Tables</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getActionBeliefs <em>Action Beliefs</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getObservation <em>Observation</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getActionTaken <em>Action Taken</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.DecisionImpl#getActualReward <em>Actual Reward</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecisionImpl extends MinimalEObjectImpl.Container implements Decision {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRewardTables() <em>Reward Tables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRewardTables()
	 * @generated
	 * @ordered
	 */
	protected EList<RewardTable> rewardTables;

	/**
	 * The cached value of the '{@link #getActionBeliefs() <em>Action Beliefs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionBeliefs()
	 * @generated
	 * @ordered
	 */
	protected EList<ActionBelief> actionBeliefs;

	/**
	 * The cached value of the '{@link #getObservation() <em>Observation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservation()
	 * @generated
	 * @ordered
	 */
	protected Observation observation;

	/**
	 * The cached value of the '{@link #getActionTaken() <em>Action Taken</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionTaken()
	 * @generated
	 * @ordered
	 */
	protected Action actionTaken;

	/**
	 * The cached value of the '{@link #getActualReward() <em>Actual Reward</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualReward()
	 * @generated
	 * @ordered
	 */
	protected RewardTableRow actualReward;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.DECISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.DECISION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RewardTable> getRewardTables() {
		if (rewardTables == null) {
			rewardTables = new EObjectContainmentEList<RewardTable>(RewardTable.class, this, StormlogPackage.DECISION__REWARD_TABLES);
		}
		return rewardTables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ActionBelief> getActionBeliefs() {
		if (actionBeliefs == null) {
			actionBeliefs = new EObjectContainmentEList<ActionBelief>(ActionBelief.class, this, StormlogPackage.DECISION__ACTION_BELIEFS);
		}
		return actionBeliefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Observation getObservation() {
		if (observation != null && observation.eIsProxy()) {
			InternalEObject oldObservation = (InternalEObject)observation;
			observation = (Observation)eResolveProxy(oldObservation);
			if (observation != oldObservation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.DECISION__OBSERVATION, oldObservation, observation));
			}
		}
		return observation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Observation basicGetObservation() {
		return observation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setObservation(Observation newObservation) {
		Observation oldObservation = observation;
		observation = newObservation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.DECISION__OBSERVATION, oldObservation, observation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action getActionTaken() {
		if (actionTaken != null && actionTaken.eIsProxy()) {
			InternalEObject oldActionTaken = (InternalEObject)actionTaken;
			actionTaken = (Action)eResolveProxy(oldActionTaken);
			if (actionTaken != oldActionTaken) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.DECISION__ACTION_TAKEN, oldActionTaken, actionTaken));
			}
		}
		return actionTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetActionTaken() {
		return actionTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActionTaken(Action newActionTaken) {
		Action oldActionTaken = actionTaken;
		actionTaken = newActionTaken;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.DECISION__ACTION_TAKEN, oldActionTaken, actionTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RewardTableRow getActualReward() {
		if (actualReward != null && actualReward.eIsProxy()) {
			InternalEObject oldActualReward = (InternalEObject)actualReward;
			actualReward = (RewardTableRow)eResolveProxy(oldActualReward);
			if (actualReward != oldActualReward) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.DECISION__ACTUAL_REWARD, oldActualReward, actualReward));
			}
		}
		return actualReward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RewardTableRow basicGetActualReward() {
		return actualReward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActualReward(RewardTableRow newActualReward) {
		RewardTableRow oldActualReward = actualReward;
		actualReward = newActualReward;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.DECISION__ACTUAL_REWARD, oldActualReward, actualReward));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StormlogPackage.DECISION__REWARD_TABLES:
				return ((InternalEList<?>)getRewardTables()).basicRemove(otherEnd, msgs);
			case StormlogPackage.DECISION__ACTION_BELIEFS:
				return ((InternalEList<?>)getActionBeliefs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.DECISION__NAME:
				return getName();
			case StormlogPackage.DECISION__REWARD_TABLES:
				return getRewardTables();
			case StormlogPackage.DECISION__ACTION_BELIEFS:
				return getActionBeliefs();
			case StormlogPackage.DECISION__OBSERVATION:
				if (resolve) return getObservation();
				return basicGetObservation();
			case StormlogPackage.DECISION__ACTION_TAKEN:
				if (resolve) return getActionTaken();
				return basicGetActionTaken();
			case StormlogPackage.DECISION__ACTUAL_REWARD:
				if (resolve) return getActualReward();
				return basicGetActualReward();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.DECISION__NAME:
				setName((String)newValue);
				return;
			case StormlogPackage.DECISION__REWARD_TABLES:
				getRewardTables().clear();
				getRewardTables().addAll((Collection<? extends RewardTable>)newValue);
				return;
			case StormlogPackage.DECISION__ACTION_BELIEFS:
				getActionBeliefs().clear();
				getActionBeliefs().addAll((Collection<? extends ActionBelief>)newValue);
				return;
			case StormlogPackage.DECISION__OBSERVATION:
				setObservation((Observation)newValue);
				return;
			case StormlogPackage.DECISION__ACTION_TAKEN:
				setActionTaken((Action)newValue);
				return;
			case StormlogPackage.DECISION__ACTUAL_REWARD:
				setActualReward((RewardTableRow)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.DECISION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StormlogPackage.DECISION__REWARD_TABLES:
				getRewardTables().clear();
				return;
			case StormlogPackage.DECISION__ACTION_BELIEFS:
				getActionBeliefs().clear();
				return;
			case StormlogPackage.DECISION__OBSERVATION:
				setObservation((Observation)null);
				return;
			case StormlogPackage.DECISION__ACTION_TAKEN:
				setActionTaken((Action)null);
				return;
			case StormlogPackage.DECISION__ACTUAL_REWARD:
				setActualReward((RewardTableRow)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.DECISION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StormlogPackage.DECISION__REWARD_TABLES:
				return rewardTables != null && !rewardTables.isEmpty();
			case StormlogPackage.DECISION__ACTION_BELIEFS:
				return actionBeliefs != null && !actionBeliefs.isEmpty();
			case StormlogPackage.DECISION__OBSERVATION:
				return observation != null;
			case StormlogPackage.DECISION__ACTION_TAKEN:
				return actionTaken != null;
			case StormlogPackage.DECISION__ACTUAL_REWARD:
				return actualReward != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DecisionImpl
