/**
 */
package uk.ac.aston.stormlog.impl;

import com.google.common.base.Objects;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.Decision;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Measure;
import uk.ac.aston.stormlog.NFR;
import uk.ac.aston.stormlog.Observation;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Log</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getTimesliceID <em>Timeslice ID</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getDecisions <em>Decisions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getRequirements <em>Requirements</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getMeasures <em>Measures</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.LogImpl#getObservations <em>Observations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LogImpl extends MinimalEObjectImpl.Container implements Log {
	/**
	 * The default value of the '{@link #getTimesliceID() <em>Timeslice ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimesliceID()
	 * @generated
	 * @ordered
	 */
	protected static final String TIMESLICE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimesliceID() <em>Timeslice ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimesliceID()
	 * @generated
	 * @ordered
	 */
	protected String timesliceID = TIMESLICE_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDecisions() <em>Decisions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecisions()
	 * @generated
	 * @ordered
	 */
	protected EList<Decision> decisions;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> actions;

	/**
	 * The cached value of the '{@link #getRequirements() <em>Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<NFR> requirements;

	/**
	 * The cached value of the '{@link #getMeasures() <em>Measures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasures()
	 * @generated
	 * @ordered
	 */
	protected EList<Measure> measures;

	/**
	 * The cached value of the '{@link #getObservations() <em>Observations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservations()
	 * @generated
	 * @ordered
	 */
	protected EList<Observation> observations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.LOG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTimesliceID() {
		return timesliceID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTimesliceID(String newTimesliceID) {
		String oldTimesliceID = timesliceID;
		timesliceID = newTimesliceID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.LOG__TIMESLICE_ID, oldTimesliceID, timesliceID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Decision> getDecisions() {
		if (decisions == null) {
			decisions = new EObjectContainmentEList<Decision>(Decision.class, this, StormlogPackage.LOG__DECISIONS);
		}
		return decisions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Action> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<Action>(Action.class, this, StormlogPackage.LOG__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NFR> getRequirements() {
		if (requirements == null) {
			requirements = new EObjectContainmentEList<NFR>(NFR.class, this, StormlogPackage.LOG__REQUIREMENTS);
		}
		return requirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Measure> getMeasures() {
		if (measures == null) {
			measures = new EObjectContainmentEList<Measure>(Measure.class, this, StormlogPackage.LOG__MEASURES);
		}
		return measures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Observation> getObservations() {
		if (observations == null) {
			observations = new EObjectContainmentEList<Observation>(Observation.class, this, StormlogPackage.LOG__OBSERVATIONS);
		}
		return observations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Measure getMeasure(final String measureName) {
		EList<Measure> _measures = this.getMeasures();
		for (final Measure m : _measures) {
			String _name = m.getName();
			boolean _equals = Objects.equal(_name, measureName);
			if (_equals) {
				return m;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action getAction(final String actionName) {
		EList<Action> _actions = this.getActions();
		for (final Action a : _actions) {
			String _name = a.getName();
			boolean _equals = Objects.equal(_name, actionName);
			if (_equals) {
				return a;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StormlogPackage.LOG__DECISIONS:
				return ((InternalEList<?>)getDecisions()).basicRemove(otherEnd, msgs);
			case StormlogPackage.LOG__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
			case StormlogPackage.LOG__REQUIREMENTS:
				return ((InternalEList<?>)getRequirements()).basicRemove(otherEnd, msgs);
			case StormlogPackage.LOG__MEASURES:
				return ((InternalEList<?>)getMeasures()).basicRemove(otherEnd, msgs);
			case StormlogPackage.LOG__OBSERVATIONS:
				return ((InternalEList<?>)getObservations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.LOG__TIMESLICE_ID:
				return getTimesliceID();
			case StormlogPackage.LOG__DECISIONS:
				return getDecisions();
			case StormlogPackage.LOG__ACTIONS:
				return getActions();
			case StormlogPackage.LOG__REQUIREMENTS:
				return getRequirements();
			case StormlogPackage.LOG__MEASURES:
				return getMeasures();
			case StormlogPackage.LOG__OBSERVATIONS:
				return getObservations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.LOG__TIMESLICE_ID:
				setTimesliceID((String)newValue);
				return;
			case StormlogPackage.LOG__DECISIONS:
				getDecisions().clear();
				getDecisions().addAll((Collection<? extends Decision>)newValue);
				return;
			case StormlogPackage.LOG__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends Action>)newValue);
				return;
			case StormlogPackage.LOG__REQUIREMENTS:
				getRequirements().clear();
				getRequirements().addAll((Collection<? extends NFR>)newValue);
				return;
			case StormlogPackage.LOG__MEASURES:
				getMeasures().clear();
				getMeasures().addAll((Collection<? extends Measure>)newValue);
				return;
			case StormlogPackage.LOG__OBSERVATIONS:
				getObservations().clear();
				getObservations().addAll((Collection<? extends Observation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.LOG__TIMESLICE_ID:
				setTimesliceID(TIMESLICE_ID_EDEFAULT);
				return;
			case StormlogPackage.LOG__DECISIONS:
				getDecisions().clear();
				return;
			case StormlogPackage.LOG__ACTIONS:
				getActions().clear();
				return;
			case StormlogPackage.LOG__REQUIREMENTS:
				getRequirements().clear();
				return;
			case StormlogPackage.LOG__MEASURES:
				getMeasures().clear();
				return;
			case StormlogPackage.LOG__OBSERVATIONS:
				getObservations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.LOG__TIMESLICE_ID:
				return TIMESLICE_ID_EDEFAULT == null ? timesliceID != null : !TIMESLICE_ID_EDEFAULT.equals(timesliceID);
			case StormlogPackage.LOG__DECISIONS:
				return decisions != null && !decisions.isEmpty();
			case StormlogPackage.LOG__ACTIONS:
				return actions != null && !actions.isEmpty();
			case StormlogPackage.LOG__REQUIREMENTS:
				return requirements != null && !requirements.isEmpty();
			case StormlogPackage.LOG__MEASURES:
				return measures != null && !measures.isEmpty();
			case StormlogPackage.LOG__OBSERVATIONS:
				return observations != null && !observations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StormlogPackage.LOG___GET_MEASURE__STRING:
				return getMeasure((String)arguments.get(0));
			case StormlogPackage.LOG___GET_ACTION__STRING:
				return getAction((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (timesliceID: ");
		result.append(timesliceID);
		result.append(')');
		return result.toString();
	}

} //LogImpl
