/**
 */
package uk.ac.aston.stormlog.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import uk.ac.aston.stormlog.NFR;
import uk.ac.aston.stormlog.RewardTableThreshold;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reward Table Threshold</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableThresholdImpl#getValue <em>Value</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableThresholdImpl#getNfr <em>Nfr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RewardTableThresholdImpl extends MinimalEObjectImpl.Container implements RewardTableThreshold {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final double VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected double value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNfr() <em>Nfr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNfr()
	 * @generated
	 * @ordered
	 */
	protected NFR nfr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RewardTableThresholdImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.REWARD_TABLE_THRESHOLD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(double newValue) {
		double oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.REWARD_TABLE_THRESHOLD__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NFR getNfr() {
		if (nfr != null && nfr.eIsProxy()) {
			InternalEObject oldNfr = (InternalEObject)nfr;
			nfr = (NFR)eResolveProxy(oldNfr);
			if (nfr != oldNfr) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.REWARD_TABLE_THRESHOLD__NFR, oldNfr, nfr));
			}
		}
		return nfr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NFR basicGetNfr() {
		return nfr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNfr(NFR newNfr) {
		NFR oldNfr = nfr;
		nfr = newNfr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.REWARD_TABLE_THRESHOLD__NFR, oldNfr, nfr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_THRESHOLD__VALUE:
				return getValue();
			case StormlogPackage.REWARD_TABLE_THRESHOLD__NFR:
				if (resolve) return getNfr();
				return basicGetNfr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_THRESHOLD__VALUE:
				setValue((Double)newValue);
				return;
			case StormlogPackage.REWARD_TABLE_THRESHOLD__NFR:
				setNfr((NFR)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_THRESHOLD__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case StormlogPackage.REWARD_TABLE_THRESHOLD__NFR:
				setNfr((NFR)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_THRESHOLD__VALUE:
				return value != VALUE_EDEFAULT;
			case StormlogPackage.REWARD_TABLE_THRESHOLD__NFR:
				return nfr != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //RewardTableThresholdImpl
