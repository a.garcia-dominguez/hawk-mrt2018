/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reward Table Threshold</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.RewardTableThreshold#getValue <em>Value</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.RewardTableThreshold#getNfr <em>Nfr</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableThreshold()
 * @model
 * @generated
 */
public interface RewardTableThreshold extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableThreshold_Value()
	 * @model unique="false"
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.RewardTableThreshold#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

	/**
	 * Returns the value of the '<em><b>Nfr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nfr</em>' reference.
	 * @see #setNfr(NFR)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableThreshold_Nfr()
	 * @model
	 * @generated
	 */
	NFR getNfr();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.RewardTableThreshold#getNfr <em>Nfr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nfr</em>' reference.
	 * @see #getNfr()
	 * @generated
	 */
	void setNfr(NFR value);

} // RewardTableThreshold
