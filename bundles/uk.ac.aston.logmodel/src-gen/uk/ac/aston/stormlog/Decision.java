/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getName <em>Name</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getRewardTables <em>Reward Tables</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getActionBeliefs <em>Action Beliefs</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getObservation <em>Observation</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getActionTaken <em>Action Taken</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Decision#getActualReward <em>Actual Reward</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision()
 * @model
 * @generated
 */
public interface Decision extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Decision#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Reward Tables</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.RewardTable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reward Tables</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_RewardTables()
	 * @model containment="true"
	 * @generated
	 */
	EList<RewardTable> getRewardTables();

	/**
	 * Returns the value of the '<em><b>Action Beliefs</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.ActionBelief}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Beliefs</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_ActionBeliefs()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActionBelief> getActionBeliefs();

	/**
	 * Returns the value of the '<em><b>Observation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observation</em>' reference.
	 * @see #setObservation(Observation)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_Observation()
	 * @model
	 * @generated
	 */
	Observation getObservation();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Decision#getObservation <em>Observation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observation</em>' reference.
	 * @see #getObservation()
	 * @generated
	 */
	void setObservation(Observation value);

	/**
	 * Returns the value of the '<em><b>Action Taken</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Taken</em>' reference.
	 * @see #setActionTaken(Action)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_ActionTaken()
	 * @model
	 * @generated
	 */
	Action getActionTaken();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Decision#getActionTaken <em>Action Taken</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Taken</em>' reference.
	 * @see #getActionTaken()
	 * @generated
	 */
	void setActionTaken(Action value);

	/**
	 * Returns the value of the '<em><b>Actual Reward</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Reward</em>' reference.
	 * @see #setActualReward(RewardTableRow)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getDecision_ActualReward()
	 * @model
	 * @generated
	 */
	RewardTableRow getActualReward();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Decision#getActualReward <em>Actual Reward</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Reward</em>' reference.
	 * @see #getActualReward()
	 * @generated
	 */
	void setActualReward(RewardTableRow value);

} // Decision
