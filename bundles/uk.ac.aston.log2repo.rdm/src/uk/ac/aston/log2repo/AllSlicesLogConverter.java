package uk.ac.aston.log2repo;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.ac.aston.stormlog.Log;

/**
 * Converts a STORM all-slices log into a Git repository.
 */
public class AllSlicesLogConverter {

	private final Map<String, TimeSliceEntry> rawLog;
	private final RDMConfiguration rdmConfig;
	private final File repoFolder;

	public AllSlicesLogConverter(File rdmFile, File jsonLog, File repoFolder) throws JsonParseException, JsonMappingException, IOException, IllegalStateException, GitAPIException {
		final ObjectMapper om = new ObjectMapper();

		this.rdmConfig = om.readValue(rdmFile, RDMConfiguration.class);
		this.rawLog = om.readValue(jsonLog, new TypeReference<Map<String, TimeSliceEntry>>() {});

		Git.init().setDirectory(repoFolder).call();
		this.repoFolder = repoFolder;
	}

	public void convert() throws NumberFormatException, IOException, NoFilepatternException, GitAPIException {
		final long start = System.nanoTime();

		for (Entry<String, TimeSliceEntry> entry : rawLog.entrySet()) {
			final long startEntry = System.nanoTime();
			File modelFile = convert(entry.getKey(), entry.getValue());

			try (Git repo = Git.open(repoFolder)) {
				repo.add().addFilepattern(modelFile.getName()).call();
				repo.commit().setMessage("Committing slice " + entry.getKey()).call();
			}
			final long endEntry = System.nanoTime();
			final long startMillis = (endEntry - start) / 1_000_000;
			final long startEntryMillis = (endEntry - startEntry) / 1_000_000;

			System.out.println(String.format(   
				"Converted slice %s (took %d ms) - %d ms since start",
				entry.getKey(), startEntryMillis, startMillis));
		}
	}

	private File convert(String timesliceID, TimeSliceEntry sliceInfo) throws IOException {
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());

		final File modelFile = new File(repoFolder, "log.xmi");
		final Resource r = rs.createResource(URI.createFileURI(modelFile.getAbsolutePath()));
		final Log log = new TimeSliceConverter().convert(rdmConfig, timesliceID, sliceInfo);

		r.getContents().add(log);
		r.save(null);
		r.unload();

		return modelFile;
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Usage: path/to/rdm.json path/to/log.json path/to/newrepo");
			System.exit(1);
		}
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println(args[2]);
		try {
			// Delete and recreate the repository
			final File newRepo = new File(args[2]);
			if (newRepo.exists()) {
				FileUtils.deleteDirectory(newRepo);
			} else {
				newRepo.mkdirs();
			}

			new AllSlicesLogConverter(new File(args[0]), new File(args[1]), newRepo).convert();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
