package uk.ac.aston.log2repo;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.ActionBelief;
import uk.ac.aston.stormlog.Decision;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Measure;
import uk.ac.aston.stormlog.NFR;
import uk.ac.aston.stormlog.NFRSatisfaction;
import uk.ac.aston.stormlog.Observation;
import uk.ac.aston.stormlog.RangeMeasurement;
import uk.ac.aston.stormlog.RewardTable;
import uk.ac.aston.stormlog.RewardTableRow;
import uk.ac.aston.stormlog.RewardTableThreshold;
import uk.ac.aston.stormlog.StormlogFactory;
import uk.ac.aston.stormlog.Threshold;

/**
 * Converts a single time slice to its model format.
 */
public class TimeSliceConverter {

	private static final StormlogFactory FACTORY = StormlogFactory.eINSTANCE;

	private NFR nfrMEC;
	private NFR nfrMR;
	private NFR nfrMP;
	private Measure metricREC;
	private Measure metricNCC;
	private Measure metricTTW;
	private Action actionMST;
	private Action actionRT;
	
	private static final boolean[][] REWARDTABLE_TRUTHVALUES = {
		{ true, true, true }, 
		{ true, true, false}, 
		{ true, false, true }, 
		{ true, false, false },
		{ false, true, true },
		{ false, true, false },
		{ false, false, true },
		{ false, false, false}
	};

	private static final int MAX_OBS_CODE = 27;

	private static final int positionByCodeREC(int obsCode) {
		assert obsCode >= 0;
		assert obsCode < MAX_OBS_CODE;
		return obsCode / 9;
	}

	private static final int positionByCodeNCC(int obsCode) {
		assert obsCode >= 0;
		assert obsCode < MAX_OBS_CODE;
		return (obsCode / 3) % 3;
	}

	private static final int positionByCodeTTW(int obsCode) {
		assert obsCode >= 0;
		assert obsCode < MAX_OBS_CODE;
		return obsCode % 3;
	}

	public Log convert(RDMConfiguration rdmConfig, String timesliceID, TimeSliceEntry sliceInfo) {
		final Log log = FACTORY.createLog();
		log.setTimesliceID(timesliceID);		
		addActions(log);
		addMeasure(log);
		Observation obs = addObservation(log, sliceInfo);
		addNonFunctionalRequirements(rdmConfig, log);
		Decision dec = addDecision(log, sliceInfo, rdmConfig);
		dec.setObservation(obs);
		return log;
	}

	protected Decision addDecision(final Log log, TimeSliceEntry sliceInfo, RDMConfiguration rdmConfig) {
		final Decision dec = FACTORY.createDecision();
		dec.setName("Main");
		addRewardTable(sliceInfo, log, dec, rdmConfig.nfrThresholds);

		log.getDecisions().add(dec);
/*
		addNFRBeliefPre(dec, nfrMEC, sliceInfo.getCurrentBeliefMEC(), sliceInfo.getCurrentStateMEC());
		addNFRBeliefPre(dec, nfrMR, sliceInfo.getCurrentBeliefMR(), sliceInfo.getCurrentStateMR());
		addNFRBeliefPre(dec,nfrMP,sliceInfo.getCurrentBeliefMP(), sliceInfo.getCurrentStateMP());
		addNFRBeliefPost(dec, nfrMEC, sliceInfo.getUpdatedBeliefMEC(), sliceInfo.getArrivedStateMEC());
		addNFRBeliefPost(dec, nfrMR, sliceInfo.getUpdatedBeliefMR(), sliceInfo.getArrivedStateMR());
		addNFRBeliefPost(dec, nfrMP, sliceInfo.getUpdatedBeliefMP(), sliceInfo.getArrivedStateMP());
*/
		addActionBelief(dec, actionRT, sliceInfo.getEstimatedValueRT());
		addActionBelief(dec, actionMST, sliceInfo.getEstimatedValueMST());

		switch (sliceInfo.getSelectedAction()) {
		case "RT":
			dec.setActionTaken(actionRT);
			break;
		case "MST":
			dec.setActionTaken(actionMST);
			break;
		default:
			throw new IllegalArgumentException("Unknown action " + sliceInfo.getSelectedAction());
		}

		return dec;
	}

	protected Observation addObservation(final Log log, TimeSliceEntry sliceInfo) {
		Observation obs = FACTORY.createObservation();
		obs.setDescription(sliceInfo.getObservationDescription());
		obs.setProbability(sliceInfo.getObservationProbability());
		log.getObservations().add(obs);

		final int obsCode = sliceInfo.getCurrentObservationCode();
		if (obsCode >= 0 && obsCode < MAX_OBS_CODE) {
			addMeasurement(obs, metricREC, positionByCodeREC(obsCode));
			addMeasurement(obs, metricNCC, positionByCodeNCC(obsCode));
			addMeasurement(obs, metricTTW, positionByCodeTTW(obsCode));
		} else if (obsCode != -1) {
			System.err.println("Warning: observation code " + obsCode + " is out of range and not -1");
		}

		return obs;
	}

	protected void addMeasurement(Observation obs, final Measure measure, final int measurementPosition) {
		RangeMeasurement measurementREC = FACTORY.createRangeMeasurement();
		measurementREC.setMeasure(measure);
		measurementREC.setPosition(measurementPosition);
		obs.getMeasurements().add(measurementREC);
	}

	protected void addActionBelief(final Decision dec, final Action action, final double ev) {
		ActionBelief beliefRT = FACTORY.createActionBelief();
		beliefRT.setAction(action);
		beliefRT.setEstimatedValue(ev);
		dec.getActionBeliefs().add(beliefRT);
	}
/*
	protected void addNFRBeliefPre(final Decision dec, final NFR nfr, final double prob, final boolean state) {
		final NFRBelief belief = FACTORY.createNFRBelief();
		belief.setNfr(nfr);
		belief.setEstimatedProbability(prob);
		belief.setSatisfied(state);
		dec.getNfrBeliefsPre().add(belief);
	}
	protected void addNFRBeliefPost(final Decision dec, final NFR nfr, final double prob, final boolean state) {
		final NFRBelief belief = FACTORY.createNFRBelief();
		belief.setNfr(nfr);
		belief.setEstimatedProbability(prob);
		belief.setSatisfied(state);
		dec.getNfrBeliefsPost().add(belief);
	}
*/
	protected void addRewardTable(TimeSliceEntry sliceInfo, final Log log, final Decision dec, final double[] thresholds) {
		RewardTable rewardTable = FACTORY.createRewardTable();
		dec.getRewardTables().add(rewardTable);
		addRewardsForAction(rewardTable, actionMST, REWARDTABLE_TRUTHVALUES, sliceInfo.getCurrentRewards()[0]);
		addRewardsForAction(rewardTable, actionRT, REWARDTABLE_TRUTHVALUES, sliceInfo.getCurrentRewards()[1]);	
		addRewardTableThreshold(rewardTable,nfrMEC,thresholds[0]);
		addRewardTableThreshold(rewardTable,nfrMR,thresholds[1]);
		addRewardTableThreshold(rewardTable,nfrMP,thresholds[2]);
	}

	protected void addRewardsForAction(RewardTable rewardTable, final Action action, final boolean[][] truthValues, final double[] rewards) {
		for (int iRewardMST = 0; iRewardMST < rewards.length; ++iRewardMST) {
			final RewardTableRow row = FACTORY.createRewardTableRow();
			row.setAction(action);
			addNFRSatisfaction(row, nfrMEC, truthValues[iRewardMST][0]);
			addNFRSatisfaction(row, nfrMR, truthValues[iRewardMST][1]);
			addNFRSatisfaction(row, nfrMP, truthValues[iRewardMST][2]);
		//	row.setValue(rewards[iRewardMST]);
			rewardTable.getRows().add(row);
		}
	}

	protected void addNFRSatisfaction(final RewardTableRow row, final NFR nfr, final boolean satisfied) {
		NFRSatisfaction satisfactionMEC = FACTORY.createNFRSatisfaction();
		satisfactionMEC.setNfr(nfr);
		satisfactionMEC.setSatisfied(satisfied);
		row.getSatisfactions().add(satisfactionMEC);
	}

	protected void addMeasure(final Log log) {
		metricREC = FACTORY.createMeasure();
		metricREC.setName("Ranges of Energy Consumption");
		addThreshold(metricREC, "x");
		addThreshold(metricREC, "y");
		addThreshold(metricREC, "z");
		log.getMeasures().add(metricREC);

		metricNCC = FACTORY.createMeasure();
		metricNCC.setName("Number of Concurrent Connections");
		addThreshold(metricNCC, "r");
		addThreshold(metricNCC, "s");
		log.getMeasures().add(metricNCC);
		
		metricTTW = FACTORY.createMeasure();
		metricTTW.setName("Total time writing");
		addThreshold(metricTTW, "f");
		addThreshold(metricTTW, "g");
		log.getMeasures().add(metricTTW);
	}

	protected void addNonFunctionalRequirements(RDMConfiguration rdmConfig, final Log log) {
		nfrMEC = addNFR(log, "Minimization of Energy Consumption", metricREC);
		nfrMR = addNFR(log, "Maximization of Reliability", metricNCC);
		nfrMP =addNFR(log, "Maximization of Performance", metricTTW);
	}

	protected void addThreshold(final Measure metric, final String thresholdName) {
		final Threshold threshold = FACTORY.createThreshold();
		threshold.setName(thresholdName);
		metric.getThresholds().add(threshold);
	}

	protected void addActions(final Log log) {
		actionMST = addAction(log, "Minimum Spanning Tree Topology");
		actionRT = addAction(log, "Redundant Topology");
	}

	protected Action addAction(final Log log, final String actionName) {
		Action action = FACTORY.createAction();
		action.setName(actionName);
		log.getActions().add(action);
		return action;
	}

	protected NFR addNFR(final Log log, final String nfrName,final Measure measure) {
		final NFR nfr = FACTORY.createNFR();
		nfr.setName(nfrName);
		nfr.getMeasures().add(measure);	 
		log.getRequirements().add(nfr);
		return nfr;
	}
	protected void addRewardTableThreshold(final RewardTable rewardTable, final NFR nfr, final double threshold) {
		final RewardTableThreshold jugglingThreshold = FACTORY.createRewardTableThreshold();
		jugglingThreshold.setValue(threshold);
		jugglingThreshold.setNfr(nfr);
		rewardTable.getThresholds().add(jugglingThreshold);		
	}

}
