package uk.ac.aston.log2repo;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.ac.aston.stormlog.Log;

/**
 * Converts a single-slice STORM JSON log into an XMI model.
 */
public class SingleSliceLogConverter {

	private static final String LOG_XMI_FILENAME = "log.xmi";

	private final Map<String, TimeSliceEntry> rawLog;

	private final RDMConfiguration rdmConfig;

	public SingleSliceLogConverter(File rdmFile, File jsonLog)
			throws JsonParseException, JsonMappingException, IOException {
		final ObjectMapper om = new ObjectMapper();

		this.rdmConfig = om.readValue(rdmFile, RDMConfiguration.class);

		// Note: this is using the single-slice file produced by RDM, not the all-slices file
		this.rawLog = om.readValue(jsonLog,
			new TypeReference<Map<String, TimeSliceEntry>>() {});
	}
 
	public File convert(File targetFolder) throws NumberFormatException, IOException {
		final long startEntry = System.nanoTime();
		final Entry<String, TimeSliceEntry> lastEntry = getLastEntry();
		final File modelFile = convert(Integer.valueOf(lastEntry.getKey()), lastEntry.getValue(), targetFolder);
		final long endEntry = System.nanoTime();
		final long startEntryMillis = (endEntry - startEntry) / 1_000_000;
		System.out.println(String.format(
			"Converted slice %s to file, took %d ms and the value is > %s",
			lastEntry.getKey(), startEntryMillis,lastEntry.getValue()));
		
		if (modelFile.exists()) {
			String path = modelFile.getAbsolutePath();
			System.out.println("File created at " + path);
		}

		return modelFile;
	}

	private Entry<String, TimeSliceEntry> getLastEntry() {
		Entry<String, TimeSliceEntry> lastEntry = null;
		for (Entry<String, TimeSliceEntry> entry : rawLog.entrySet()) {
			lastEntry = entry;
		}
		return lastEntry;
	}

	private File convert(int timeSlice, TimeSliceEntry sliceInfo, File targetFolder) throws IOException {
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		final File modelFile = new File(targetFolder, LOG_XMI_FILENAME);
		final Resource r = rs.createResource(URI.createFileURI(modelFile.getAbsolutePath()));
		final Log log = convert(timeSlice, sliceInfo);

		r.getContents().add(log);
		r.save(null);
		r.unload();

		return modelFile;
	}

	public Log convert() {
		final long startEntryTime = System.nanoTime();
		final Entry<String, TimeSliceEntry> lastEntry = getLastEntry();
		final Log log = convert(Integer.valueOf(lastEntry.getKey()), lastEntry.getValue());
		final long endEntryTime = System.nanoTime();

		final long startEntryMillis = (endEntryTime - startEntryTime) / 1_000_000;
		System.out.println(String.format(
			"Converted slice %s in memory, took %d ms",
			lastEntry.getKey(), startEntryMillis));

		return log;
	}

	private Log convert(int timeSlice, TimeSliceEntry sliceInfo) {
		return new TimeSliceConverter().convert(rdmConfig, "" + timeSlice, sliceInfo);
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Usage: path/to/rdm.json path/to/log.json path/to/log.xmi");
			System.exit(1);
		}
		try {
			final File rdmJsonFile = new File(args[0]);
			final File logJsonFile = new File(args[1]);
			final File xmiFile = new File(args[2]);

			// Delete and recreate the repository
			if (xmiFile.exists()) {
				FileUtils.deleteDirectory(xmiFile);
			} else {
				xmiFile.mkdirs();
			}

			new SingleSliceLogConverter(rdmJsonFile, logJsonFile).convert(xmiFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
