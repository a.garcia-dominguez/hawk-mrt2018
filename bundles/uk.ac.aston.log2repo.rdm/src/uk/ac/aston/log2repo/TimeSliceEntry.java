package uk.ac.aston.log2repo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Bean for entries in the log. 
 */
@JsonIgnoreProperties({"ave_mec_previous_action", "ave_mr_previous_action", "rewards","acu_rewards_undiscounted","acu_rewards_discounted"})
public class TimeSliceEntry {
	
	/////New Beliefs
	@JsonProperty("updated_belief_mec_true_(s')")
	private double updatedBeliefMEC;
	@JsonProperty("updated_belief_mr_true_(s')")
	private double updatedBeliefMR;	
	@JsonProperty("updated_belief_mp_true_(s')")
	private double updatedBeliefMP;
	

	//////New States
	@JsonProperty("current_mec_state(s)")
	private boolean currentStateMEC;
	@JsonProperty("current_mr_state(s)")
	private boolean currentStateMR;
	@JsonProperty("current_mp_state(s)")
	private boolean currentStateMP;
	
	@JsonProperty("arrived_mec_state_(s')")
	private boolean arrivedStateMEC;
	@JsonProperty("arrived_mr_state_(s')")
	private boolean arrivedStateMR;
	@JsonProperty("arrived_mp_state_(s')")
	private boolean arrivedStateMP;
	
	///////New reward
	@JsonProperty("step_reward")
	private double stepReward;
	////////////////////////////////////////////////
	
	@JsonProperty("current_belief_mec_true_(s)")
	private double currentBeliefMEC;
	@JsonProperty("current_belief_mr_true_(s)")
	private double currentBeliefMR;
	@JsonProperty("current_belief_mp_true_(s)")
	private double currentBeliefMP;
	
	
	@JsonProperty("observation_code")
	private int currentObservationCode;
	@JsonProperty("current_rewards")
	private double[][] currentRewards;

	@JsonProperty("ev.mst")
	private double estimatedValueMST;

	@JsonProperty("ev.rt")
	private double estimatedValueRT;

	@JsonProperty("not_poor_step_nro_")
	private double notPoorStepNRO;

	@JsonProperty("flagUpdatedRewards")
	private int flagUpdatedRewards;
	@JsonProperty("observation_description")
	private String observationDescription;
	@JsonProperty("observation_probability")
	private double observationProbability;
	@JsonProperty("selected_action")
	private String selectedAction;
	
	public double getStepReward() {
		return stepReward;
	}
	public void setStepReward(double stepReward) {
		this.stepReward = stepReward;
	}
	public double getCurrentBeliefMEC() {
		return currentBeliefMEC;
	}

	public void setCurrentBeliefMEC(double currentBeliefMEC) {
		this.currentBeliefMEC = currentBeliefMEC;
	}

	public double getCurrentBeliefMR() {
		return currentBeliefMR;
	}

	public void setCurrentBeliefMR(double currentBeliefMR) {
		this.currentBeliefMR = currentBeliefMR;
	}
	
	public double getCurrentBeliefMP() {
		return currentBeliefMP;
	}
	public void setCurrentBeliefMP(double currentBeliefMP) {
		this.currentBeliefMP = currentBeliefMP;
	}
	
	/////////////////////////////////////////////
	
	public double getUpdatedBeliefMEC() {
		return updatedBeliefMEC;
	}
	public void setUpdatedBeliefMEC(double updatedBeliefMEC) {
		this.updatedBeliefMEC = updatedBeliefMEC;
	}
	public double getUpdatedBeliefMR() {
		return updatedBeliefMR;
	}
	public void setUpdatedBeliefMR(double updatedBelief) {
		this.updatedBeliefMR = updatedBelief;
	}

	public double getUpdatedBeliefMP() {
		return updatedBeliefMP;
	}
	public void setUpdatedBeliefMP(double updatedBeliefMP) {
		this.updatedBeliefMP = updatedBeliefMP;
	}
	
	//////////////////////State///////////////////////
	public boolean getCurrentStateMEC() {
		return currentStateMEC;
	}
	public void setCurrentStateMEC(boolean currentStateMEC) {
		this.currentStateMEC = currentStateMEC;
	}
	public boolean getCurrentStateMR() {
		return currentStateMR;
	}
	public void setCurrentStateMR(boolean currentStateMR) {
		this.currentStateMR = currentStateMR;
	}
	public boolean getCurrentStateMP() {
		return currentStateMP;
	}
	public void setCurrentStateMP(boolean currentStateMP) {
		this.currentStateMP = currentStateMP;
	}
	/////////////////////////////////////////////
	public boolean getArrivedStateMEC() {
		return arrivedStateMEC;
	}
	public void setArrivedStateMEC(boolean ArrivedStateMEC) {
		this.arrivedStateMEC = ArrivedStateMEC;
	}
	public boolean getArrivedStateMR() {
		return arrivedStateMR;
	}
	public void setArrivedStateMR(boolean ArrivedStateMR) {
		this.arrivedStateMR = ArrivedStateMR;
	}
	public boolean getArrivedStateMP() {
		return arrivedStateMP;
	}
	public void setArrivedStateMP(boolean ArrivedStateMP) {
		this.arrivedStateMP = ArrivedStateMP;
	}
	/////////////////////////////////////////////
	
	
	public int getCurrentObservationCode() {
		return currentObservationCode;
	}

	public void setCurrentObservationCode(int currentObservationCode) {
		this.currentObservationCode = currentObservationCode;
	}

	public double[][] getCurrentRewards() {
		return currentRewards;
	}

	public void setCurrentRewards(double[][] currentRewards) {
		this.currentRewards = currentRewards;
	}

	public double getEstimatedValueMST() {
		return estimatedValueMST;
	}

	public void setEstimatedValueMST(double estimatedValueMST) {
		this.estimatedValueMST = estimatedValueMST;
	}

	public double getEstimatedValueRT() {
		return estimatedValueRT;
	}

	public void setEstimatedValueRT(double estimatedValueRT) {
		this.estimatedValueRT = estimatedValueRT;
	}

	public double getNotPoorStepNRO() {
		return notPoorStepNRO;
	}

	public void setNotPoorStepNRO(double notPoorStepNRO) {
		this.notPoorStepNRO = notPoorStepNRO;
	}

	public int getFlagUpdatedRewards() {
		return flagUpdatedRewards;
	}

	public void setFlagUpdatedRewards(int flagUpdatedRewards) {
		this.flagUpdatedRewards = flagUpdatedRewards;
	}

	public String getObservationDescription() {
		return observationDescription;
	}

	public void setObservationDescription(String observationDescription) {
		this.observationDescription = observationDescription;
	}

	public double getObservationProbability() {
		return observationProbability;
	}

	public void setObservationProbability(double observationProbability) {
		this.observationProbability = observationProbability;
	}

	public String getSelectedAction() {
		return selectedAction;
	}

	public void setSelectedAction(String selectedAction) {
		this.selectedAction = selectedAction;
	}

}
