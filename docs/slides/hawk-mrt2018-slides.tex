\documentclass[10pt]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\graphicspath{{}{figures/}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\pgfplotsset{compat=1.12}
\usepgfplotslibrary{dateplot}

\usetikzlibrary{calc,fit,patterns}
\usepackage[absolute,overlay]{textpos}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\usepackage{xcolor}
\usepackage{listings}
\lstset{columns=fullflexible}
\lstdefinelanguage{EOL}{
morekeywords={delete,import,for,while,in,and,or,self,operation,return,def,var,throw,if,new,else,transaction,abort,
break,breakAll,continue,assert,assertError,not, switch, case, default},
sensitive=true,
morecomment=[l]{//},
morecomment=[l]{--},
morecomment=[s]{/*}{*/},
morecomment=[s]{-*}{*-},
morestring=[b]",
morestring=[b]',
showstringspaces=false
}
\input{JSONFormat.tex}

\lstnewenvironment{java}{\lstset{language=Java,
		frame=tb,
        tabsize=3,
        morekeywords={implies, in, result},
        basicstyle=\footnotesize,
        keywordstyle=\bfseries,
        ndkeywordstyle=\bfseries,
        commentstyle=\itshape,
		morecomment=[l]{--},
        stringstyle=\ttfamily,
		showspaces=false,
        flexiblecolumns,
        literate={->}{$\to$}{2} {--}{-$\,$-}{2} {<=}{$\le$}{2} {>=}{$\ge$}{2} {<>}{$<\,>$}{3},
        sensitive, extendedchars, texcl}}{}

\lstnewenvironment{ocl}{\lstset{language=[decorative]OCL,
	frame=tb,
	tabsize=3,
	morekeywords={implies,result,flatten,body,init,OrderedSet,Tuple,TupleType,def,attr,oclIsUndefined,oclIsInvalid,OclState,let,in},
	basicstyle=\footnotesize,
	keywordstyle=\bfseries,
	ndkeywordstyle=\bfseries,
	commentstyle=\itshape,
	stringstyle=\ttfamily,
	showspaces=false,
	flexiblecolumns,
	literate={->}{$\to$}{2} {--}{-$\,$-}{2} {<=}{$\le$}{2} {>=}{$\ge$}{2} {<>}{$<\,>$}{3},
	sensitive, extendedchars, texcl}}{}


\lstdefinelanguage{gremlin}{
morekeywords={as,def,fill,filter,groupCount,has,idx,inE,inV,is,label,length,match,outE,outV,v,values},
sensitive=true,
morecomment=[l]{//}
}

% "page cs" coordinate system
% From http://tex.stackexchange.com/questions/89588/
%
% Defining a new coordinate system for the page:
%
% --------------------------
% |(-1,1)    (0,1)    (1,1)|
% |                        |
% |(-1,0)    (0,0)    (1,0)|
% |                        |
% |(-1,-1)   (0,-1)  (1,-1)|
% --------------------------
\makeatletter
\def\parsecomma#1,#2\endparsecomma{\def\page@x{#1}\def\page@y{#2}}
\tikzdeclarecoordinatesystem{page}{
    \parsecomma#1\endparsecomma
    \pgfpointanchor{current page}{north east}
    % Save the upper right corner
    \pgf@xc=\pgf@x%
    \pgf@yc=\pgf@y%
    % save the lower left corner
    \pgfpointanchor{current page}{south west}
    \pgf@xb=\pgf@x%
    \pgf@yb=\pgf@y%
    % Transform to the correct placement
    \pgfmathparse{(\pgf@xc-\pgf@xb)/2.*\page@x+(\pgf@xc+\pgf@xb)/2.}
    \expandafter\pgf@x\expandafter=\pgfmathresult pt
    \pgfmathparse{(\pgf@yc-\pgf@yb)/2.*\page@y+(\pgf@yc+\pgf@yb)/2.}
    \expandafter\pgf@y\expandafter=\pgfmathresult pt
}
\makeatother
% Draws a grid for easier referencing of page cs values
\newcommand{\printtikzpagegrid}{
  \tiny
  \begin{tikzpicture}[overlay,remember picture,every node/.style={inner sep=.1em,draw=black!20,fill=white}]
    \foreach \x in {0,...,9} {
      \foreach \y in {0,...,9} {
        \node at (page cs:0.\x,0.\y) {};
        \node at (page cs:0.\x,-0.\y) {};
        \node at (page cs:-0.\x,0.\y) {};
        \node at (page cs:-0.\x,-0.\y) {};
      }
    }
    \node at (page cs:0,0) {0,0};
    \node at (page cs:0.5,0.5) {.5,.5};
    \node at (page cs:0.5,-0.5) {.5,-.5};
    \node at (page cs:-0.5,0.5) {-.5,.5};
    \node at (page cs:-0.5,-0.5) {-.5,-.5};
    \node at (page cs:1,1) {1,1};
    \node at (page cs:1,-1) {1,-1};
    \node at (page cs:-1,1) {-1,1};
    \node at (page cs:-1,-1) {-1,-1};
  \end{tikzpicture}
  \normalsize
}

\usepackage[space]{grffile}
\usepackage{textcomp}

\title{Reflecting on the past and the present with temporal graph-based models}
%\subtitle{A modern beamer theme}
\date{MRT'18, 14 October 2018}
\author{A. García-Domínguez, N. Bencomo, Luis H. García Paucar}
%\institute{Center for modern beamer themes}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

%% \begin{frame}{Table of contents}
%%   \setbeamertemplate{section in toc}[sections numbered]
%%   \tableofcontents[hideallsubsections]
%% \end{frame}

\section{Motivation}

\begin{frame}{Gaining trust on a self-adaptive system (SAS)}

  \begin{block}{Emergent behaviour in self-adaptive systems}
    \begin{itemize}
    \item Self-adaptive systems build a model of how their part of the world
      works, and how they can best meet their goals
    \item This results in emergent behaviour: it may be correct and meet the
      goals, but it could seem not immediately ``obvious'' to users or even
      developers
    \end{itemize}
  \end{block}

  \begin{block}{Good self-explanation is needed}
    \begin{itemize}
    \item Developers can use it to debug the system and check if it meets
      certain safety criteria
    \item Users can ask questions and gain more confidence about why the SAS did
      something at a certain moment
    \end{itemize}
  \end{block}
  
\end{frame}

\begin{frame}{Reflection in self-adaptive systems}

  \begin{block}{Natural ``compression'' in most systems}
    \begin{itemize}
    \item Many SAS work by building a neural network / Bayesian network / other
      model from observations and decisions.
    \item In a way, this packs the history of the system into knowledge, but it
      may be ``lossy'', i.e. impossible to retrieve the original system state.
    \item It may be based on uncertain information: what if we kept track of how
      uncertain it was?
    \item What if we could look back at an explicit history of the system?
    \end{itemize}
  \end{block}

  \begin{block}{Adding reflective capabilities to systems}
    \begin{itemize}
    \item What could we do if the system could ask explicit questions about its
      own past behaviour and its consequences?
    \item Could we make the system make better decisions?
    \item Could we make the system make more understandable decisions?
    \end{itemize}
  \end{block}

  % Self-adaptive systems generally build a model of their surroundings and the
  % working of their domain. This may be a mathematical model (e.g. a neural
  % network, a Markov chain), a more object-oriented model (e.g. EMF-based
  % models), or a mix of the two.

  % Many models naturally ``compress'' some of the past history, losing some
  % level of detail, limiting how far we can look back, or making it more
  % difficult to give explanations.

  % It would be useful to have an easier way to access past observations/system
  % models and ask direct questions, without having to undo that compression
  % performed by the original model.

\end{frame}

\begin{frame}{Types of traces}

  \begin{block}{Traditional approach: textual output as we go along}
    \begin{itemize}
    \item Plain logging framework, just printing whatever we decide
    \item Easy to implement, easy for devs to read --- hard to parse!
    \end{itemize}
  \end{block}

  \begin{block}{Better: structured traces}
    \begin{itemize}
    \item Generate an XML/JSON snapshot of system state by timeslice
    \item Slightly more work, but computer-friendly
    \item What about history, though?
    \end{itemize}
  \end{block}

  \begin{block}{We want trace models that are...}
    \begin{itemize}
    \item Based on a complete and reusable metamodel
    \item Stored in a way that allows ``travelling in time''
    \item Answering questions about system history concisely
    \item Presenting answers in an accesible way
    \end{itemize}
  \end{block}

  % The ``traditional'' approach is to produce detailed execution/sensor traces,
  % so we can look back. For each timeslice that the SAS operates in, we produce
  % a snapshot of the system. If something looks odd, we go in and we try to
  % find out things by scanning up and down for the odd isntance.

  % This is easy to implement, but you may lose the original structure and the
  % links between the environment and the self-adaptation may be lost.

  % MRT-based SAS already have rich representations internally: why not use them
  % as the history? They are conceptually graphs --- we can reduce the problem
  % to keeping track of an evolving graph and asking questions about it.

  % Ideally, we don't want to go to all this trouble for every system - we want
  % to make all of this as reusable as possible across MRT-based SASs.

\end{frame}

\section{Proposal}

\begin{frame}{Problem-independent trace models}

  \begin{block}{Many self-adaptive systems...}
    \begin{itemize}
    \item Quantize time into \emph{slices}
    \item Make observations and analyse them
    \item Plan future behaviour
    \item Execute plans
    \end{itemize}

    This is common in those based on the MAPE-K architecture.
  \end{block}

  \begin{block}{Can we define a domain-specific language for their state of mind?}
    \begin{itemize}
    \item We have a first version
    \item So far, tried it only on one SAS, though
    \item See next slide!
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Problem-independent trace metamodel}

  \begin{center}
    \includegraphics[width=\textwidth,height=.8\textheight,keepaspectratio]{stormlog class diagram}
  \end{center}

  The SAS is trying to achieve \alert{NFR}s by making a \alert{Decision} among
  several \alert{Action}s, guided by \alert{Observation}s of certain
  \alert{Metric}s.

\end{frame}

\begin{frame}{What is a temporal graph?}

  \begin{columns}[t]
    \column{.48\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\columnwidth]{kostakos}
      Contact sequence (Kostakos)
    \end{figure}
    \column{.52\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\columnwidth]{hartmann}
      Efficient storage (Hartmann)
    \end{figure}
  \end{columns}

  \vspace{1em}

  \begin{block}{Conflicting definitions}
    \vspace{-.1em}
    \begin{itemize}
    \item Kostakos thinks about graphs of ``contacts'' between entities
    \item Hartmann focuses on efficient storage of a versioned graph
    \end{itemize}
  \end{block}

  \vspace{-.5em}
  \pause

  \begin{block}{In this paper...}
    \vspace{.2em}
    We use the vision of temporal graphs from Hartmann, in its Greycat TGDB implementation.
  \end{block}

\end{frame}


\begin{frame}{Transparent temporal graph storage}

  \begin{block}{Object-oriented models are time-evolving graphs}
    \begin{itemize}
    \item Objects = nodes (labelled by type)
    \item References = edges
    \item Nodes and edges have attributes (object/reference fields)
    \item Overall: model = \emph{labelled attributed graph} that evolves over time
    \end{itemize}
  \end{block}

  \begin{block}{Hawk + Greycat: our implementation}
    \begin{itemize}
    \item Hawk is our open-source heterogeneous model indexing framework:
      \url{https://github.com/mondo-project/mondo-hawk}
    \item Watches over the models and mirrors all history into a Greycat
      temporal graph, applying changes incrementally
    \item SAS models can be used as-is if based on EMF
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Reusable time-aware query language}

  \begin{block}{So far...}
    \begin{itemize}
    \item We have a representation of our SAS' state of mind
    \item We keep track of the full history of this representation
    \item We want to start asking questions --- how do we express them?
    \end{itemize}
  \end{block}

  \begin{block}{Our approach}
    \begin{itemize}
    \item Extend an existing query language, with good tool support
    \item Hawk already supported the Epsilon Object Language ($\sim$ OCL + JS)
    \item More info: \url{http://eclipse.org/epsilon}
    \item Added temporal extensions based on Rose and Segev's work in the 90s with
      \emph{history objects} for object-oriented databases
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Time-aware primitives for the Hawk EOL dialect}

  \begin{columns}[T]
    \column{.5\textwidth}
    \begin{block}{Model element history}
      \begin{itemize}
      \item Limited lifespan
      \item Instance-based identity
      \item New version when state changes
      \end{itemize}
    \end{block}

    \column{.5\textwidth}
    \begin{block}{Type history}
      \begin{itemize}
      \item Unlimited lifespan
      \item Name-based identity
      \item New version when instances are created or deleted
      \end{itemize}
    \end{block}
  \end{columns}

  \begin{table}
    \centering
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{Operation} & \multicolumn{1}{c}{Syntax} \\
      \midrule
      All versions (newest to oldest) & \verb#x.versions# \\
      Versions within a range & \verb#x.getVersionsBetween(from, to)# \\
      Versions from timepoint (incl.) &
      \verb#x.getVersionsFrom(from)# \\
      Versions up to timepoint (incl.) &
      \verb#x.getVersionsUpTo(from)# \\
      Earliest / latest version & \verb#x.earliest#, \verb#x.latest# \\
      Next / previous version & \verb#x.next#, \verb#x.prev#/\verb#x.previous# \\
      Version timepoint & \verb#x.time# \\
      \bottomrule
    \end{tabular}
  \end{table}

\end{frame}

\begin{frame}{Reusable visualizations}

  \begin{block}{Potential examples}
    \begin{itemize}
    \item Key instants in the SAS, with links to main changes in behaviour
    \item Predefined ``why'' questions for common queries:
      \begin{itemize}
      \item Why was it doing this at this time?
      \item Why did it stop doing the previous action?
      \item Why was this change considered good?
      \item What was the evaluation process like?
      \item Why was the evaluation process configured that way?
      \end{itemize}
    \item Visualizations would be bundled with the reusable representation and
      the appropriate time-aware queries
    \end{itemize}
  \end{block}

  This is still work in progress!

\end{frame}

\section[Case study]{Case study: Remote Data Mirroring}

\begin{frame}{Case study: Remote Data Mirroring}
  \begin{columns}
    \column{.5\textwidth}
    \includegraphics[width=\columnwidth]{RDM_POMDP}

    \column{.5\textwidth}
    \begin{block}{Concept}
      \vspace{.1em}
      SAS that protects against data loss by storing copies on servers.
    \end{block}

    \begin{block}{Configuration}
      \begin{itemize}
      \item 2 topologies: min.\ spanning tree (MST), redundant (RT)
      \item 2 NFRs: max reliability (MR), min energy consumption (MEC)
      \item 2 monitoring variables: energy use, \# of connections
      \end{itemize}
    \end{block}

    \begin{block}{Goal}
      \vspace{.1em}
      Switch between MST and RT to meet MR and MEC.
    \end{block}
  \end{columns}

\end{frame}

\begin{frame}[fragile]{Turning JSON traces into a temporal graph}
  \begin{center}
    \lstinputlisting[language=JSON,basicstyle=\footnotesize]{logi1000j0a-excerpt.json}
  \end{center}
  \vspace{-1.75em}

  \begin{overprint}
    \onslide<1>
    \begin{block}{Steps taken}
      \vspace{.25em}
      \begin{enumerate}
      \item Collected JSON traces from existing RDM SAS for 1000 time slices
      \item Transformed traces to execution trace models
      \item Turned sequence of trace models into a Subversion repository
      \item Told Hawk to index all model revisions into a Greycat TGDB
      \end{enumerate}
    \end{block}

    \onslide<2>
    \begin{block}{Current study limitations}
      \vspace{1em}
      \begin{itemize}
      \item No changes were made to the SAS in this study
      \item The current case study focuses on \emph{forensic analysis}
      \end{itemize}
    \end{block}

    \onslide<3>
    \begin{block}{Ideally...}
      \vspace{1em}
      \begin{itemize}
      \item The SAS would simply work off from a model indexable by Hawk, and
        Hawk + SAS would operate concurrently
      \item The SAS could ask Hawk about the history of the model to make
        history-aware decisions
      \end{itemize}
    \end{block}
  \end{overprint}
\end{frame}

\begin{frame}{Queries for developers}

  \begin{itemize}
  \item Self-explanations must be tailored to the target audience:
    \begin{description}
    \item[Developer] Wants to know things about the system
    \item[User] Wants to know if the NFRs were met, and how
    \item[System] Wants to find similar patterns to current observations, what went well, and not so well
    \end{description}
  \item Self-explanations must focus on the intended use.
  \item As an example, for forensic analysis:
    \begin{itemize}
    \item Is a certain desired property holding over time?
    \item If not, when did the system misbehave?
    \item Why did it misbehave then?
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Queries for developers: did reward values change?}
  Let's start with a simple one:

  \lstset{frame=tb}
  \begin{overprint}
    \onslide<1>
    \lstinputlisting[language=EOL,escapechar=@]{queries/didRewardsChange.eol}

    \onslide<2>
    \lstinputlisting[language=EOL,escapechar=@]{queries/didRewardsChange2.eol}

    \onslide<3>
    \lstinputlisting[language=EOL,escapechar=@]{queries/didRewardsChange3.eol}

    \onslide<4>
    \lstinputlisting[language=EOL,escapechar=@]{queries/didRewardsChange4.eol}

    \onslide<5>
    \lstinputlisting[language=EOL,escapechar=@]{queries/didRewardsChange5.eol}
  \end{overprint}

  \begin{enumerate}
  \item<1- | alert@1> RewardTableRow.latest returns the latest version of the type node, with
    all the available instances (they are created once and never deleted),
  \item<2- | alert@2> .all returns all instances of the type at that point in time,
  \item<3- | alert@3> .collect visits each instance and produces a sequence of results,
  \item<4- | alert@4> .versions.size measures how many times the rewards changed,
  \item<5- | alert@5> .max() returns the number of times the most active reward table changed.
  \end{enumerate}

  \begin{overprint}
    \onslide<5>
    The most active row changed a total of 442 times in our traces.
  \end{overprint}
\end{frame}

\begin{frame}[fragile]{Queries for developers: distribution of shifts in reward values}
  \framesubtitle{Going from ``did it change?'' to ``how did it change?''}

  \lstset{language=EOL,escapechar=@,basicstyle=\footnotesize}
  \begin{overprint}
    \onslide<1>
    \lstinputlisting{queries/rewardSwingDistribution.eol}

    \onslide<2>
    \lstinputlisting{queries/rewardSwingDistribution2.eol}

    \onslide<3>
    \lstinputlisting{queries/rewardSwingDistribution3.eol}

    \onslide<4>
    \lstinputlisting{queries/rewardSwingDistribution4.eol}

    \onslide<5>
    \lstinputlisting{queries/rewardSwingDistribution5.eol}
  \end{overprint}

  \begin{enumerate}
  \item<1- | alert@1> RewardTableRow.latest.all returns all reward table rows,
  \item<2- | alert@2> .collect visits each instance and produces a sequence of results,
  \item<3- | alert@3> .versions returns all versions from newest to oldest,
  \item<4- | alert@4> .prev compares adjacent values up to the second oldest version,
  \item<5- | alert@5> .min()/.max()/.average() compute descriptive statistics.
    16 lines of EOL check that the SAS kept shifts bounded to $\pm 0.034$.
  \end{enumerate}

\end{frame}

\begin{frame}{Queries for developers: more examples}

  \begin{block}{Thrashing between two actions?}
    \begin{itemize}
    \item Is the SAS constantly changing actions?
    \item 33 lines of EOL later, we found a sequence of 8 timeslices in which
      the SAS was switching between RT and MST.
    \item Would this be acceptable behaviour for a SAS?
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Unintuitive inferences?}
    \begin{itemize}
    \item Does the SAS think at some point that a NFR is not being met, even if
      the observation says it is within range?
    \item Wrote query in 22 lines of EOL --- there are 18 time slices in which
      the SAS thought the MEC NFR was not being met, even though there is low
      energy usage.
    \item Is this obvious? We may need to look at neighbouring observations to
      find out.
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Queries for users: enabling exploration}

  \begin{block}{Differences from developer-oriented queries}
    \begin{itemize}
    \item These are problem-centric, rather than solution-centric
    \item Were my NFRs met? If not, what was done about it, and why?
    \item Queries may need to be organised in a way that promotes exploration
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Initial dashboard-style query: overall system health}
    \begin{itemize}
    \item We defined a query in 10 lines that measures number of versions in
      which each NFR is met and unmet
    \item MEC: SAS had 670 belief levels as ``met'' out of 888 
    \item MR: SAS had 665 belief levels as ``met'' out of 888
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Queries for users: timeline views}

\begin{lstlisting}[caption={Excerpt of output from query},breaklines=true,basicstyle=\ttfamily\footnotesize,frame=tb]
[[{Maximization of Reliability=false, Minimization of Energy Consumption=false}, 1, 1532385574820, REC LOWER X AND NCC GREATER S, Redundant Topology],
[{Maximization of Reliability=true, Minimization of Energy Consumption=true}, 1, 1532385575022, REC IN Y_Z AND NCC GREATER S, Minimum Spanning Tree Topology],
[{Maximization of Reliability=true, Minimization of Energy Consumption=false}, 1, 1532385575166, REC LOWER X AND NCC GREATER S, Minimum Spanning Tree Topology],
...]
\end{lstlisting}

\begin{itemize}
\item Queries bring together beliefs and observations at decision points:
  \begin{itemize}
  \item Beliefs can be simplified to yes/no if deemed useful.
  \item Observations can be translated from the solution domain (e.g. ``code 3'') back to the problem domain (``high use of energy'').
  \end{itemize}
\item We can simplify the timeline to points where the decision changed.
\item The paper shows a 35-line query that does this.
\end{itemize}

\end{frame}

\appendix

\begin{frame}{Conclusion and future work}

  \begin{block}{Key points}
    \begin{itemize}
    \item Text logs, structured traces not enough for good self-explanation
    \item For reusable self-explanation and reflection in SASs, we need:
      \begin{itemize}
      \item A reusable trace metamodel
      \item A transparent way to store versioned models as temporal graphs
      \item A reusable time-aware querying language
      \item A set of reusable visualizations based on the trace metamodel
      \end{itemize}
    \item We showed the approach on a small case study (RDM)
    \end{itemize}
  \end{block}

  \begin{block}{Future lines of work}
    \begin{itemize}
    \item Build visualizations!
    \item Create a taxonomy of typical questions to ask from a SAS
    \item Try out the trace metamodel on more SAS, and allow ``profiling''
    \item Extend the time-aware primitives to cover linear temporal logic
    \item Leverage time-awareness for better simulations and decision-making
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[standout]
  Thank you!

  \IfFileExists{\jobname-copy.pdf}{%
    \begin{center}
      \foreach \i in {1,8,10,13,14,16,19,30,35} {
        \hyperlink{page.\i}{\includegraphics[width=0.3\linewidth, page=\i]{\jobname-copy.pdf}}
      }
    \end{center}
  }{\typeout{No Thumbnails included}}

\end{frame}

%% \begin{frame}[allowframebreaks]{References}
%%   \bibliography{bibliography}
%%   \bibliographystyle{alpha}
%% \end{frame}

\end{document}
